<?php
/**
* emailtemplates.php - Email Templates
*
* @author     Sergey V Musenko <info@ws123.net>
* @copyright  Segmob.com
* @date       2017-03-30
* @version    0.1
*/


$EMAILSIGN = '<a href="'.DOMAINURL.'"><img src="https://www.24monetize.com/assets/img/signature.png" title="24Monetize. Department: Affiliate Team"></a>';

$EMAILTPLS = [

	'reg_user' => [ // Just Signed Up, keys: FULLNAME, FROMEMAIL
		'subj' => 'Welcome to '.AP_BRAND,
		'body' => "<html><body>
<h3>Hello {FULLNAME},</h3>
<p>Thank you for joining <a href='".DOMAINURL."'>".AP_BRAND."</a> - Lottery Affiliate Program Africa. Together we will transform the way traffic is monetized across Africa!</p>
<p>In order to get approved and get started simply answer to this email telling us how you plan to generate traffic to 24Lottos? What website(s) or promotional methods do you intend to use to promote 24Lottos?</p>
<p>You can reach us by email <a href='mailto:{FROMEMAIL}'>{FROMEMAIL}</a></p>
<p>Thank you!<br/>
Best wishes,</p>
$EMAILSIGN
</p>
</body></html>",
	],

	'reg_admin' => [ //  Just Signed Up - to Admin, keys: USERDETAILS
		'subj' => 'Hey Admin! New affiliate',
		'body' => "<html><body>
<h3>Hi Admin,</h3>
<p>New affiliate joined:<br/>
{USERDETAILS}
</p>
<p><b>Have a nice day!</b><br/>
<a href='".DOMAINURL."'>".AP_BRAND."</a></p>
</body></html>",
	],

	'reg_reject' => [ // Rejected, keys: FULLNAME, FROMEMAIL
		'subj' => 'Application Rejected',
		'body' => "<html><body>
<h3>Hello {FULLNAME},</h3>
<p>Thank you for submitting your application form to <a href='".DOMAINURL."'>".DOMAIN."</a></p>
<p>We regret to inform you that we have declined your application based on the information you provided. If you feel we have misunderstood your ideas, please send us an email at <a href='mailto:{FROMEMAIL}'>{FROMEMAIL}</a> with a detailed description of your ideas other than the one earlier provided.</p>
<p>Best wishes,</p>
$EMAILSIGN
</body></html>",
	],

	'reg_approve' => [ // Approved, keys: FULLNAME, USERNAME, FROMEMAIL
		'subj' => 'Approved',
		'body' => "<html><body>
<h3>Hi {FULLNAME},</h3>
<p>Thank you for submitting your application form to <a href='".DOMAINURL."'>".DOMAIN."</a></p>
<p>You have been approved and can now login using the following link:</p>
<p>Your username: <a href='".DOMAINURL."'>{USERNAME}</a></p>
<p>Inside your affiliate account you'll be able to do the following:
<ol>
	<li><a href='".DOMAINURL."/banners-logos'>Create and change</a> your affiliate links to promote the most popular lotteries.</li>
	<li><a href='".DOMAINURL."/my-sales'>Track your stats - general</a>, as well as <a href='".DOMAINURL."'>mobile</a> and <a href='".DOMAINURL."'>by users</a> geo’s;</li>
	<li>Use <a href='".DOMAINURL."/banners-logos'>referral links and promos</a> for attracting affiliates like you are and track your <a href='".DOMAINURL."'>income and referrals' count</a>.</li>
</ol>
</p>
<p>For more information, please email <a href='mailto:{FROMEMAIL}'>{FROMEMAIL}</a></p>
<p>Best of luck!</p>
$EMAILSIGN
</body></html>",
	],
];

function getSubj( $eml, $keys=false ) {
	global $EMAILTPLS;
	if( empty($eml) ) return '';
	if( is_array($keys) ) return str_replace( array_keys($keys), array_values($keys), $EMAILTPLS[$eml]['subj'] );
	return $EMAILTPLS[$eml]['subj'];
}
function getBody( $eml, $keys=false ) {
	global $EMAILTPLS;
	if( empty($eml) ) return '';
	if( is_array($keys) ) return str_replace( array_keys($keys), array_values($keys), $EMAILTPLS[$eml]['body'] );
	return $EMAILTPLS[$eml]['body'];
}

/* // TEST EMAILS TEMPLATE
//include_once( $_SERVER['DOCUMENT_ROOT'].'/assets/comp/emailtemplates.php');
$keys = [
	'{USERNAME}' => 'username',
	'{FULLNAME}' => 'Ucwords Fullname',
	'{FROMEMAIL}'=> 'site@email.com',
	'{USERDETAILS}' => "<table width='100%' border='1' cellpadding='2' cellspacing='0'>
<tr><td width='80' nowrap>User Name:</td><td>username</td></tr>
<tr><td>Full Name:</td><td>fullname</td></tr>
<tr><td>Email:</td><td>to@email</td></tr>
<tr><td>Date:</td><td>".date('Y-m-d H:i')."</td></tr>
<tr><td>IP:</td><td>".$_SERVER['REMOTE_ADDR']."</td></tr>
</table>",
];
foreach( $EMAILTPLS as $k=>$v ) {
	$subject = getSubj( $k, $keys );
	$message = getBody( $k, $keys );
	echo "<div style='border:1px solid #333'>$k</div>Subj: $subject<br>$message\n\n\n\n";
}
exit;
*/