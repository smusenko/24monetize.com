<div class="footer-guest">
	<div class="container">
		<div class="row nopadding">
			<div class="col-lg-12 nopadding">
				<a href='/privacy-policy'>Privacy Policy</a> &nbsp; &nbsp;
				<a href='/terms-and-conditions'>Terms & Conditions</a> &nbsp; &nbsp;
				<nobr>Copyright &copy <?= date('Y') ?> 24Monetize</nobr>
			</div>
		</div>
	</div><!-- /.container -->
</div>

<!-- jQuery -->
<script src="/assets/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="/assets/js/bootstrap.min.js"></script>

<!-- Base Theme JS -->
<script src="/assets/js/base.js"></script>

<?= GoogleAnalytics ?>
