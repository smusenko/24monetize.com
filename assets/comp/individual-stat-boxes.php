<div class="row">
	<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
		<div class="stat-box stat-success">
			<a href="my-sales">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-dollar"></i>
				</div>
				<div class="stat-data">
					<h2><?= total_sales_period( $start_date, $end_date, $affiliate_filter ); ?>
						<span class="stat-info"><?php echo $lang['SALES']; ?>
							<span class="small-text">(selected period)</span>
						</span>
					</h2>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
		<div class="stat-box stat-danger">
			<a href="my-sales">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-money"></i>
				</div>
				<div class="stat-data">
					<h2><?= affiliate_earnings_period( $start_date, $end_date, $affiliate_filter ); ?>
						<span class="stat-info"><?php echo $lang['EARNINGS']; ?>
							<span class="small-text">(selected period)</span>
						</span>
					</h2>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-4 col-md-3 col-sm-6 col-xs-12">
		<div class="stat-box stat-default">
			<a href="traffic">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-rocket"></i>
				</div>
				<div class="stat-data">
					<h2><?= total_referrals_period( $start_date, $end_date, $affiliate_filter ); ?>
						<span class="stat-info"><?php echo $lang['REFERRED']; ?>
							<span class="small-text">(selected period)</span>
						</span>
					</h2>
				</div>
			</a>
		</div>
	</div>
</div>