<div class="row">
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="stat-box stat-success">
			<a href="#">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-dollar"></i>
				</div>
				<div class="stat-data">
					<h2><?= total_sales_period( $start_date, $end_date, $owner ); ?>
						<span class="stat-info"><?php echo $lang['TOTAL_SALES']; ?>
							<span class="small-text">(for period below)</span>
						</span>
					</h2>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
		<div class="stat-box stat-danger">
			<a href="#">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-money"></i>
				</div>
				<div class="stat-data">
					<h2><?= affiliate_earnings_period( $start_date, $end_date, $owner ); ?>
						<span class="stat-info"><?php echo $lang['AFFILIIATE_EARNINGS']; ?>
							<span class="small-text">(for period below)</span>
						</span>
					</h2>
				</div>
			</a>
		</div>
	</div>
</div>