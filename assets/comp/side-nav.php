<?php /* $pg - see load.php */ ?>
<!-- Side Wrapper -->
<div id="side-wrapper">
<div class="side-heading" style='position:relative;top:68px'>Main Menu</div>

<ul class="side-nav">

<li class="side-danger menu-item<?= $pg=='dashboard' ? ' active':'' ?>">
	<a href="dashboard">
		<i class="fa-desktop"></i> <?= $lang['DASHBOARD']; ?>
	</a>
</li>

<?php if( $admin_user == '1' ) { ?>
	<li class="side-danger menu-item<?= $pg=='affiliates' ? ' active':'' ?>">
		<a href="affiliates">
			<i class="fa-users"></i> <?= $lang['AFFILIATES']; ?>
		</a>
	</li>
	<li class="side-danger menu-item<?= $pg=='advanced-reports' ? ' active':'' ?>">
		<a href="advanced-reports">
			<i class="fa-chart-line"></i> <?= $lang['ADVANCED_REPORTS']; ?>
		</a>
	</li>
	<li class="side-danger menu-item<?= $pg=='revenue-comision' ? ' active':'' ?>">
		<a href="revenue-comision">
			<i class="fa-award"></i> <?= $lang['REVENUE_COMISSION']; ?>
		</a>
	</li>
	<li class="side-danger menu-item<?= $pg=='traffic' ? ' active':'' ?>">
		<a href="traffic">
			<i class="fa-rocket"></i> <?= $lang['TRAFFIC']; ?>
		</a>
	</li>
	<?php
	if( mt_on() ) { ?>
		<li class="side-danger menu-item<?= $pg=='multi-tier' ? ' active':'' ?>">
			<a href="multi-tier">
				<i class="fa-sitemap"></i> <?= $lang['SUB_AFF_COMMISION'] /*'MULTI_TIER_COMMISSIONS'*/; ?>
			</a>
		</li>
	<?php } ?>
	<li class="side-danger menu-item">
		<a href="#" data-toggle="collapse" data-target="#cs">
			<i class="fa-calc"></i> <?= $lang['COMMISSION_SETTINGS']; ?>
		</a>
		<ul id="cs" <?= strpos($pg,'commissions')===false ? 'class="collapse"':''?> style='background:#3d3d3d'>
			<li class="<?= $pg=='fixed-commissions' ? ' active':'' ?>">
				<a href="fixed-commissions"><?= $lang['FIXED_COMMISSIONS']; ?></a>
			</li>
			<li class="<?= $pg=='sales-volume-commissions' ? ' active':'' ?>">
				<a href="sales-volume-commissions"><?= $lang['SALES_VOLUME_COMMISSIONS']; ?></a>
			</li>
			<li class="<?= $pg=='cpc-commissions' ? ' active':'' ?>">
				<a href="cpc-commissions"><?= $lang['CPC_COMMISSIONS']; ?></a>
			</li>
			<li class="<?= $pg=='recurring-commissions' ? ' active':'' ?>">
				<a href="recurring-commissions"><?= $lang['RECURRING_COMMISSIONS']; ?></a>
			</li>
			<li class="<?= $pg=='multi-tier-commissions' ? ' active':'' ?>">
				<a href="multi-tier-commissions"><?= $lang['SUB_AFF_COMMISION']; ?></a>
			</li>
			<li class="<?= $pg=='lead-commissions' ? ' active':'' ?>">
				<a href="lead-commissions"><?= $lang['LEAD_COMMISSIONS']; ?></a>
			</li>
		</ul>
	</li>
	<li class="side-danger menu-item<?= $pg=='settings' ? ' active':'' ?>">
		<a href="settings">
			<i class="fa-cog"></i> <?= $lang['WEBSITE_INTEGRATION']; ?>
		</a>
	</li>
	<li class="side-danger menu-item<?= $pg=='payouts' ? ' active':'' ?>">
		<a href="payouts">
			<i class="fa-credit-card money"></i> <?= $lang['PAYOUTS']; ?>
		</a>
	</li>

	<li class="side-danger mobile_only menu-item<?= $pg=='profile' ? ' active':'' ?>">
		<a href="profile">
			<i class="fa-user"></i> <?= $lang['ACCOUNT_PROFILE']; ?>
		</a>
	</li>
	<li class="side-danger mobile_only menu-item<?= $pg=='user-management' ? ' active':'' ?>">
		<a href="user-management">
			<i class="fa-user-md"></i> <?= $lang['USER_MANAGEMENT'] ?>
		</a>
	</li>
	<li class="side-danger mobile_only menu-item<?= $pg=='my-payouts' ? ' active':'' ?>">
		<a href="data-functions/logout">
			<i class="fa-logout"></i> <?= $lang['LOGOFF']; ?>
		</a>
	</li>

	<div class='side-top-affiliates'>
		<div class="side-heading"><br><?= $lang['TOP_AFFILIATES']; ?></div>
		<?= top_affiliates_list(); ?>
	</div>
	<?php /* ?>

	<?php if( lc_on() ) { ?>
		<li class="side-danger menu-item<?= $pg=='leads' ? ' active':'' ?>">
			<a href="leads">
				<i class="fa-user"></i> <?= $lang['LEADS']; ?>
			</a>
		</li>
	<?php } ?>
	<li class="side-danger menu-item<?= $pg=='products' ? ' active':'' ?>">
		<a href="products">
			<i class="fa-truck"></i> <?= $lang['PRODUCTS']; ?>
		</a>
	</li>
	<li class="side-danger menu-item<?= $pg=='sales-profits' ? ' active':'' ?>">
		<a href="sales-profits">
			<i class="fa-basket"></i> <?= $lang['SALES']; ?>
		</a>
	</li>
	<?php $rc_on = rc_on();
	if( $rc_on == '1' ) { ?>
		<li class="side-danger menu-item<?= $pg=='recurring-sales' ? ' active':'' ?>">
			<a href="recurring-sales">
				<i class="fa-arrows-cw"></i> <?= $lang['RECURRING_COMMISSIONS']; ?>
			</a>
		</li>
	<?php } ?>
	<li class="side-danger menu-item<?= $pg=='referral-traffic' ? ' active':'' ?>">
		<a href="referral-traffic">
			<i class="fa-rocket"></i> <?= $lang['REFERRAL_TRAFFIC']; ?>
		</a>
	</li>
	<li class="side-danger menu-item<?= $pg=='banners-logos' ? ' active':'' ?>">
		<a href="banners-logos">
			<i class="fa-picture"></i> <?= $lang['BANNERS_AND_LOGOS']; ?>
		</a>
	</li>
	<?php */ ?>


<?php } else if( $user_status!=1 ) { /* NOT APPROVED USER */ ?>
	<li class="side-danger menu-item<?= $pg=='advanced-reports' ? ' active':'' ?>">
		<a href="contact">
			<i class="fa-help-circled"></i> <?= $lang['CONTACT']; ?>
		</a>
	</li>


<?php } else { ?>
	<?php /* ?>
	<li class="side-danger menu-item<?= $pg=='leads' ? ' active':'' ?>">
		<a href="leads">
			<i class="fa-user"></i> <?= $lang['MY_LEADS']; ?>
		</a>
	</li>
	<?php */ ?>
	<li class="side-danger menu-item<?= $pg=='advanced-reports' ? ' active':'' ?>">
		<a href="advanced-reports">
			<i class="fa-chart-line"></i> <?= $lang['ADVANCED_REPORTS']; ?>
		</a>
	</li>
	<?php /* ?>
	<li class="side-danger menu-item<?= $pg=='products' ? ' active':'' ?>">
		<a href="products">
			<i class="fa-truck"></i> <?= $lang['PRODUCTS']; ?>
		</a>
	</li>
	<li class="side-danger menu-item<?= $pg=='my-sales' ? ' active':'' ?>">
		<a href="my-sales">
			<i class="fa-chart-line"></i> <?= $lang['SALES']; ?>
		</a>
	</li>
	<?php $rc_on = rc_on();
	if( $rc_on == '1' ) { ?>
		<li class="side-danger menu-item<?= $pg=='my-recurring-sales' ? ' active':'' ?>">
			<a href="my-recurring-sales">
				<i class="fa-arrows-cw"></i> <?= $lang['RECURRING_COMMISSIONS']; ?>
			</a>
		</li>
	<?php } ?>
	<?php */ ?>
	<li class="side-danger menu-item<?= $pg=='revenue-comision' ? ' active':'' ?>">
		<a href="revenue-comision">
			<i class="fa-award"></i> <?= $lang['REVENUE_COMISSION']; ?>
		</a>
	</li>
	<?php if( mt_on() ) { ?>
		<li class="side-danger menu-item<?= $pg=='multi-tier' ? ' active':'' ?>">
			<a href="multi-tier">
				<i class="fa-sitemap"></i> <?= $lang['SUB_AFF_COMMISION'] /*'MULTI_TIER_COMMISSIONS'*/; ?>
			</a>
		</li>
	<?php } ?>
	<li class="side-danger menu-item<?= $pg=='traffic' ? ' active':'' ?>">
		<a href="traffic">
			<i class="fa-rocket"></i> <?= $lang['TRAFFIC']; ?>
		</a>
	</li>
	<li class="side-danger menu-item<?= $pg=='banners-logos' ? ' active':'' ?>">
		<a href="banners-logos">
			<i class="fa-picture"></i> <?= $lang['BANNERS_AND_LOGOS']; ?>
		</a>
	</li>

	<li class="side-danger menu-item<?= $pg=='my-payouts' ? ' active':'' ?>">
		<a href="my-payouts">
			<i class="fa-money"></i> <?= $lang['PAYOUTS']; ?>
		</a>
	</li>

	<li class="side-danger mobile_only menu-item<?= $pg=='profile' ? ' active':'' ?>">
		<a href="profile">
			<i class="fa-user"></i> <?= $lang['ACCOUNT_PROFILE']; ?>
		</a>
	</li>
	<li class="side-danger mobile_only menu-item">
		<a href="data-functions/logout">
			<i class="fa-logout"></i> <?= $lang['LOGOFF']; ?>
		</a>
	</li>
<?php } ?>

</ul>
</div><!-- End Main Navigation -->
