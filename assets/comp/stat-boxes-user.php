<?php
$cpc_on = cpc_on();
if( $cpc_on == '1' ) {
	$col = '3';
} else {
	$col = '4';
}
// show data for LOCAL period (cur month)
$_start_date = date('Y-m-01');
$_end_date = date('Y-m-d');

$_visitors = total_referrals_period( $_start_date, $_end_date, $owner );
//$_sales  = total_sales_period( $_start_date, $_end_date, $owner );
$_earnings = affiliate_earnings_period( $_start_date, $_end_date, $owner );
$_balance  = balance( $owner );
?>
<div class="row">
	<div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?>  col-sm-6 col-xs-12">
		<div class="stat-box stat-primary" title="Period: since <?= $_start_date ?>">
			<a href="traffic">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-rocket"></i>
				</div>
				<div class="stat-data">
					<h2><?= $_visitors ?>
						<span class="stat-info"><?php echo $lang['VISITORS']; ?></span>
					</h2>
				</div>
			</a>
		</div>
	</div>

	<?php if( $cpc_on=='1' ) {
		$_cpcearn  = my_total_cpc_earnings( $owner ); ?>
		<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
			<div class="stat-box stat-primary">
				<a href="traffic">
					<div class="stat-icon stat-primary hvr-bounce-in">
						<i class="fa-money"></i>
					</div>
					<div class="stat-data">
						<h2><?= $_cpcearn ?>
							<span class="stat-info"><?php echo $lang['CPC_EARNINGS']; ?></span>
						</h2>
					</div>
				</a>
			</div>
		</div>
	<?php } ?>

	<div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-6 col-xs-12">
		<div class="stat-box stat-danger" title="Period: since <?= $_start_date ?>">
			<a href="my-sales">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-money"></i>
				</div>
				<div class="stat-data">
					<h2><?= $_earnings ?>
						<span class="stat-info"><?php echo $lang['COMISSION']; ?></span>
					</h2>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-<?php echo $col; ?> col-md-<?php echo $col; ?> col-sm-6 col-xs-12">
		<div class="stat-box stat-success">
			<a href="my-sales">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-dollar"></i>
				</div>
				<div class="stat-data">
					<h2><?= $_balance  ?>
						<span class="stat-info"><?php echo $lang['BALANCE']; ?></span>
					</h2>
				</div>
			</a>
		</div>
	</div>
</div>