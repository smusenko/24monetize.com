<div class="row">
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="stat-box stat-primary">
			<a href="affiliates">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-users"></i>
				</div>
				<div class="stat-data">
					<h2><?= total_affiliates(); ?>
						<span class="stat-info"><?php echo $lang['TOTAL_AFFILIATES']; ?></span>
					</h2>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="stat-box stat-default">
			<a href="traffic">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-rocket"></i>
				</div>
				<div class="stat-data">
					<h2><?= total_referrals(); ?>
						<span class="stat-info"><?php echo $lang['VISITORS']; ?></span>
					</h2>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="stat-box stat-success">
			<a href="sales-profits">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-dollar"></i>
				</div>
				<div class="stat-data">
					<h2><?= total_sales(); ?>
						<span class="stat-info"><?php echo $lang['REVENUE']; ?></span>
					</h2>
				</div>
			</a>
		</div>
	</div>
	<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
		<div class="stat-box stat-danger">
			<a href="sales-profits">
				<div class="stat-icon hvr-bounce-in">
					<i class="fa-money"></i>
				</div>
				<div class="stat-data">
					<h2><?= affiliate_earnings(); ?>
						<span class="stat-info"><?php echo $lang['COMMISSION']; ?> </span>
					</h2>
				</div>
			</a>
		</div>
	</div>
</div>