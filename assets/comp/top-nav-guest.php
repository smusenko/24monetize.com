<div class="top-nav-guest">
	<div class="container">
		<div class="row nopadding">
			<div class="col-lg-12 nopadding">
				<button id="mobile-menu-toggle"><i class="fa fa-menu"></i></button>
				<div class='logo-wrap'><a class="navlogo" href="/"><?= site_logo(); ?></a></div>
				<a class="signup-button" href="https://affiliates.24monetize.com" data-old="login">Login</a>
				<nav class="nav-menu">
					<ul>
						<li <?= $pg=='index' ? 'class="current"':''   ?>><a href="/"><i></i>Home</a></li>
						<li <?= $pg=='why' ? 'class="current"':''     ?>><a href="why"><i></i>Why</a></li>
						<li <?= $pg=='how' ? 'class="current"':''     ?>><a href="how"><i></i>How</a></li>
						<li <?= $pg=='contact' ? 'class="current"':'' ?>><a href="contact"><i></i>Contact</a></li>
						<li <?= $pg=='signup' ? 'class="current"':''  ?>><a href="https://affiliates.24monetize.com" data-old="login"><i></i>Login</a></li>
					</ul>
				</nav>
			</div>
		</div>
	</div><!-- /.container -->
</div>
