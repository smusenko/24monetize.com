<nav class="navbar navbar-default navbar-fixed-top">
	<!-- Logo -->
	<div class="logo">
		<a href="dashboard"><?= site_logo(); ?></a>
	</div>
	<!-- End Logo -->
	<div id="navbar">
		<ul class="nav navbar-nav">
			<li class="menu-toggle">
				<a href="#menu-toggle" id="menu-toggle">
					<i class="fa fa-menu"></i>
				</a>
			</li>

		</ul>

		<!-- Start Notifications -->
		<ul class="nav navbar-nav navbar-right notifications">
<?php /*
			<li class="dropdown balance">
				<div class="control-group">
					<div class="controls">
						<span style="color:#333;">Balance:</span> <?= balance( $owner ); ?>
					</div>
				</div>
			</li>
			<li class="dropdown language">
				<div class="control-group">
					<div class="controls">
						<form method="post" action="includes/lang/change-lang">
							<div class="lang-select">
								<select onchange="this.form.submit()" name="selected_lang" class="selectpicker"
										data-width="190px">
									<option value="en" <?= $language=='en' ? 'selected="selected"':'' ?>
										data-content="<img src='assets/img/us-flag.png' class='flag-img'> English">
										English</option>
									<option value="fr" <?= $language=='fr' ? 'selected="selected"':'' ?>
										data-content="<img src='assets/img/fr-flag.png' class='flag-img'> French">
										French</option>
									<option value="de" <?= $language=='de' ? 'selected="selected"':'' ?>
										data-content="<img src='assets/img/ge-flag.png' class='flag-img'> German">
										German</option>
									<option value="es" <?= $language=='es' ? 'selected="selected"':'' ?>
										data-content="<img src='assets/img/sp-flag.png' class='flag-img'> Spanish">
										Spanish</option>
								</select>
							</div>
						</form>

					</div>
				</div>
			</li>
			<li class="dropdown language">
				<div class="control-group">
					<div class="controls">
						<form method="post" action="includes/lang/change-currency" style="margin-left:5px;">
							<div class="lang-select">
								<input type="hidden" name="redirect"
									   value="<?= $pg ?>">
								<select onchange="this.form.submit()" name="selected_currency" class="selectpicker"
										data-width="90px">
									<option value="en-US" <?php if( $_SESSION['locale'] == 'en-US' ) {
										echo 'selected="selected"';
									} ?>>USD
									</option>
									<option value="en-GB" <?php if( $_SESSION['locale'] == 'en-GB' ) {
										echo 'selected="selected"';
									} ?>>GBP
									</option>
									<option value="en-IN" <?php if( $_SESSION['locale'] == 'en-IN' ) {
										echo 'selected="selected"';
									} ?>>INR
									</option>
									<option value="de-DE" <?php if( $_SESSION['locale'] == 'de-DE' ) {
										echo 'selected="selected"';
									} ?>>EUR
									</option>
									<option value="ja_JP" <?php if( $_SESSION['locale'] == 'ja_JP' ) {
										echo 'selected="selected"';
									} ?>>JPY
									</option>
									<option value="ru_RU" <?php if( $_SESSION['locale'] == 'ru_RU' ) {
										echo 'selected="selected"';
									} ?>>RUR
									</option>
								</select>
							</div>
						</form>
					</div>
				</div>
			</li>
*/ ?>
			<!-- Start My Account Dropdown -->
			<li class="dropdown">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown" title="<?= $fullname ?>">
					<?php /* avatar( $userid ); */ ?>
					<span class="navbar-user"><span><?= ucwords( $username ); ?></span></span>
					<b class="caret"></b>
				</a>
				<ul class="nav-account dropdown-menu">
					<li>
						<a href="profile"><i class="icon-primary fa fa-fw fa-user"></i> <?= $lang['ACCOUNT_PROFILE']; ?></a>
					</li>
<?php if( $admin_user ) { ?>
					<li>
						<a href="user-management"><i class="icon-default fa fa-fw fa-user-md"></i> <?= $lang['USER_MANAGEMENT'] ?></a>
					</li>
<?php } ?>
					<li>
						<a href="data-functions/logout"><i class="icon-danger fa-logout"></i> <?= $lang['LOGOFF']; ?></a>
					</li>
				</ul>
			</li>
			<!-- End My Account Dropdown -->
		</ul><!-- End Notfications -->
	</div>
</nav><!-- End Top Navigation -->
