var handleDateRangePicker = function (Range,startDate,endDate) {
	if(!jQuery().daterangepicker) return;
	$(Range).daterangepicker({
			ranges: {
				'Today': ['today', 'today'],
				'Yesterday': ['yesterday', 'yesterday'],
				// 'Last 7 Days': [Date.today().add({ days: -6 }), 'today'],
				// 'Last 30 Days': [Date.today().add({ days: -29 }), 'today'],
				'Current Month': [Date.today().moveToFirstDayOfMonth(), Date.today().moveToLastDayOfMonth()],
				'Last Month': [Date.today().moveToFirstDayOfMonth().add({ months: -1 }), Date.today().moveToFirstDayOfMonth().add({ days: -1 })],
				'This Year': [Date.today().set({month:0,day:1}), 'today']
			},
			opens: 'left',
			format: 'dd.MM.yyyy',
			separator: ' to ',
			startDate: startDate,
			endDate: endDate,
			locale: {
				applyLabel: 'Submit',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom Range',
				daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			},
			showWeekNumbers: true,
			buttonClasses: ['btn-danger']
		},
		function (start, end) { // DATE CHANGE ACTION IS HERE!
			$(Range+' span').html(start.toString('MMMM d, yyyy') + ' - ' + end.toString('MMMM d, yyyy'));
			$(Range+' .date-form')[0].start_date.value=start.toString('yyyy-MM-dd');
			$(Range+' .date-form')[0].end_date.value=end.toString('yyyy-MM-dd');
			$(Range+' .date-form')[0].submit();
		}
	);
	$(Range+' span').html(_startDate.toString('MMMM d, yyyy') + ' - ' + _endDate.toString('MMMM d, yyyy'));
	$(Range).show();
}
