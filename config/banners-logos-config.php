<?php
/*
 * This is a Config File for "Marketing Materials"
 * used in /banners-logos.php
 * Sergey Musenko
 * 2016-11-14
 * v 0.1
 */

$LotteryBaseURL = 'https://www.24monetize.com';
$bannerPath     = 'https://www.24monetize.com/data/banners';
if( !AP_ENVIRONMENT ) $bannerPath = '/data/banners';

$BANNERS = array(
	// LOGO ------------------------------------
	[
		'type' => '24Lottos',
		'size' => 'logo',
		'link' => '%BASE',
		'image'=> '%PATH/24lottos.png',
	],
	// Button 116x48 ----------------------------
	[
		'type' => 'US - Powerball',
		'size' => '116x48',
		'link' => '%BASE/lottery/powerball',
		'image'=> '%PATH/1480696864338158015.png',
	],[
		'type' => 'US - Mega Millions',
		'size' => '116x48',
		'link' => '%BASE/lottery/mega-millions',
		'image' => '%PATH/14806968411371124696.png',
	],[
		'type' => 'Italy - SuperEnaLotto',
		'size' => '116x48',
		'link' => '%BASE/lottery/superenalotto',
		'image' => '%PATH/14806964001108263609.png',
	],[
		'type' => 'Europe - Eurojackpot',
		'size' => '116x48',
		'link' => '%BASE/lottery/eurojackpot',
		'image' => '%PATH/1480696082499320417.png',
	],[
		'type' => 'UK - EuroMillions',
		'size' => '116x48',
		'link' => '%BASE/lottery/euromillions-uk',
		'image' => '%PATH/1491463039869853055.png',
	],[
		'type' => 'Europe - Euro Millions',
		'size' => '116x48',
		'link' => '%BASE/lottery/euromillions',
		'image' => '%PATH/14806959641298020097.png',
	],[
		'type' => 'Australia - Oz Lotto',
		'size' => '116x48',
		'link' => '%BASE/lottery/oz-lotto',
		'image' => '%PATH/1480695831378231060.png',
	],[
		'type' => 'Ireland - Lotto',
		'size' => '116x48',
		'link' => '%BASE/lottery/irish-lotto',
		'image' => '%PATH/14806963711534375883.png',
	],[
		'type' => 'Germany - Lotto',
		'size' => '116x48',
		'link' => '%BASE/lottery/german-lotto',
		'image' => '%PATH/14806962811057434252.png',
	],[
		'type' => 'Spain - El Gordo',
		'size' => '116x48',
		'link' => '%BASE/lottery/el-gordo',
		'image' => '%PATH/1480696756465690827.png',
	],[
		'type' => 'France - Lotto',
		'size' => '116x48',
		'link' => '%BASE/lottery/french-loto',
		'image' => '%PATH/1480696257611055589.png',
	],[
		'type' => 'Canada - Lotto 6/49',
		'size' => '116x48',
		'link' => '%BASE/lottery/lotto-6-49',
		'image' => '%PATH/1480695917634368012.png',
	],[
		'type' => 'Brazil - Mega Sena',
		'size' => '116x48',
		'link' => '%BASE/lottery/mega-sena',
		'image' => '%PATH/14806958881080454808.png',
	],[
		'type' => 'Kenya - Lotto',
		'size' => '116x48',
		'link' => '%BASE/lottery/kenya-lotto',
		'image' => '%PATH/148069643666780496.png',
	],

	// Banner 120x600 ----------------------------
	[
		'type' => 'Europe - Eurojackpot',
		'size' => '120x600',
		'link' => '%BASE/lottery/eurojackpot',
		'image' => '%PATH/EuroJackpot_B1G2_120x600_EN.gif',
	],[
		'type' => 'Europe - Euro Millions',
		'size' => '120x600',
		'link' => '%BASE/lottery/euromillions-uk',
		'image' => '%PATH/Euromillions_B1G2_120x600_EN.gif',
	],[
		'type' => 'US - Mega Millions',
		'size' => '120x600',
		'link' => '%BASE/lottery/mega-millions',
		'image' => '%PATH/Megamillions_B1G2_120x600_EN.gif',
	],[
		'type' => 'US - Powerball',
		'size' => '120x600',
		'link' => '%BASE/lottery/powerball',
		'image'=> '%PATH/Powerball_B1G2_120x600_EN.gif',
	],[
		'type' => 'Italy - SuperEnaLotto',
		'size' => '120x600',
		'link' => '%BASE/lottery/superenalotto',
		'image' => '%PATH/SuperEnalotto_Play_120x600_EN.gif',
	],

	// Banner 160x600 ----------------------------
	[
		'type' => '24Lottos',
		'size' => '160x600',
		'link' => '%BASE',
		'image'=> '%PATH/24lottos_160x600-dark.gif',
	],

	// Banner 250x250 ----------------------------
	[
		'type' => 'US - Powerball',
		'size' => '250x250',
		'link' => '%BASE/lottery/powerball',
		'image'=> '%PATH/powerball-free.gif',
	],

	// Banner 300x250 ----------------------------
	[
		'type' => '24Lottos',
		'size' => '300x250',
		'link' => '%BASE',
		'image'=> '%PATH/24lottos_300x250.gif',
	],[
		'type' => 'Europe - Eurojackpot',
		'size' => '300x250',
		'link' => '%BASE/lottery/eurojackpot',
		'image' => '%PATH/EuroJackpot_B1G2_300x250_EN.gif',
	],[
		'type' => 'Europe - Euro Millions',
		'size' => '300x250',
		'link' => '%BASE/lottery/euromillions-uk',
		'image' => '%PATH/Euromillionst_B1G2_300x250_EN.gif',
	],[
		'type' => 'US - Mega Millions',
		'size' => '300x250',
		'link' => '%BASE/lottery/mega-millions',
		'image' => '%PATH/Megamillions_B1G2_300x250_EN.gif',
	],[
		'type' => 'US - Powerball',
		'size' => '300x250',
		'link' => '%BASE/lottery/powerball',
		'image'=> '%PATH/Powerball_B1G2_300x250_EN.gif',
	],[
		'type' => 'Italy - SuperEnaLotto',
		'size' => '300x250',
		'link' => '%BASE/lottery/superenalotto',
		'image' => '%PATH/SuperEnalotto_B1G2_300x250_EN.gif',
	],

	// Banner 728x90 ----------------------------
	[
		'type' => '24Lottos',
		'size' => '728x90',
		'link' => '%BASE',
		'image'=> '%PATH/24lottos_728x90.gif',
	],[
		'type' => 'Europe - Eurojackpot',
		'size' => '728x90',
		'link' => '%BASE/lottery/eurojackpot',
		'image' => '%PATH/EuroJackpot_B1G2_728x90_EN.gif',
	],[
		'type' => 'Europe - Euro Millions',
		'size' => '728x90',
		'link' => '%BASE/lottery/euromillions-uk',
		'image' => '%PATH/Euromillions_B1G2_728x90_EN.gif',
	],[
		'type' => 'US - Mega Millions',
		'size' => '728x90',
		'link' => '%BASE/lottery/mega-millions',
		'image' => '%PATH/Megamillions_B1G2_728x90_EN.gif',
	],[
		'type' => 'US - Powerball',
		'size' => '728x90',
		'link' => '%BASE/lottery/powerball',
		'image'=> '%PATH/Powerball_B1G2_728x90_EN.gif',
	],[
		'type' => 'Italy - SuperEnaLotto',
		'size' => '728x90',
		'link' => '%BASE/lottery/superenalotto',
		'image' => '%PATH/SuperEnalotto_B1G2_728x90_EN.gif',
	],
	// Sponsor
	[
		'type' => '24Monetize Sub Affiliate',
		'size' => 'logo',
		'link' => 'https://www.24monetize.com?sponsor',
		'image'=> 'https://www.24monetize.com/data/logos/aplogo2.png',
	],
);

// prepare data ---
$BANNERStypes = [];
$BANNERSsizes = [];
foreach( $BANNERS as $k=>$banner ) {
	$BANNERS[$k]['link']  = str_replace( '%BASE',  $LotteryBaseURL, $banner['link'] );
	$BANNERS[$k]['image'] = str_replace( '%PATH', $bannerPath, $banner['image'] );
	$BANNERS[$k]['name']  = $banner['type'].', '.$banner['size'];
	$BANNERStypes[$banner['type']] = 1;
	$BANNERSsizes[$banner['size']] = 1;
}
