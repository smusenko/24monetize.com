<?php
/*********************
 * Affiliate-Pro - CUSTOM config file
 */

$testDomains = array(
	'dev.',
	'.home',
); // parts or full domain names
$DOMAIN = $_SERVER['SERVER_NAME'];
$protocol = @$_SERVER['HTTPS']!=='off' || $_SERVER['SERVER_PORT']==443 ? 'https://':'http://';
$main_url = $protocol.$DOMAIN;
$domain_path = $protocol.$DOMAIN.'/';

define( 'AP_ENVIRONMENT', $DOMAIN == str_replace( $testDomains, '', $DOMAIN ) ? 1:0 );
define( 'DOMAIN',         $DOMAIN );
define( 'DOMAINURL',      $main_url );

define( 'APUSERSUPERPASSWORD', '24mon_Z0O9' ); // a super-password to any account; false: disabled

define( 'AP_BRAND',        '24Monetize' );
define( 'AP_CONTACTEMAIL', 'info@24monetize.com' );

define( 'AP_CAN_REGISTER',   'any' );
define( 'AP_DEFAULT_ROLE',   'member' );
define( 'AP_SECURE',         false );
define( 'AP_DEFAULT_STATUS', 0 ); // ap_members.status, 0:inactive, 1:active, 2:blocked

if( AP_ENVIRONMENT ) { // production host ---
	ini_set( 'display_errors', 'Off' );
	error_reporting( 0 );
	$VERSION = '1.01.12';
	define( 'AP_CSSVERSION', $VERSION );
	define( 'AP_JSVERSION',  $VERSION );
	// database connection
	define( 'AP_HOST',      'localhost');
	define( 'AP_DATABASE',  'db1_u24monom');
	define( 'AP_USER',      'u1_u24monom');
	define( 'AP_PASSWORD',  'oCNdnLbd');
	$_GoogleAnalytics ="<!-- Google Analytics -->
<script>
(function (i, s, o, g, r, a, m) {
	i['GoogleAnalyticsObject'] = r;
	i[r] = i[r] || function () { (i[r].q = i[r].q || []).push(arguments) }, i[r].l = 1 * new Date();
	a = s.createElement(o), m = s.getElementsByTagName(o)[0];
	a.async = 1;
	a.src = g;
	m.parentNode.insertBefore(a, m)
})(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
ga('create', 'UA-86725440-1', 'auto');
ga('send', 'pageview');
</script>";

} else { // testing host ---
	ini_set( 'display_errors', 'On' );
	error_reporting( E_ALL );
	define( 'AP_CSSVERSION','0.'.time() );
	define( 'AP_JSVERSION', '0.'.time() );
	// database connection
	define( 'AP_HOST',      'localhost' );
	define( 'AP_USER',      'mserg');
	define( 'AP_PASSWORD',  'MSerg');
	define( 'AP_DATABASE',  'affiliatepro');
	$_GoogleAnalytics = '';
}
define( 'GoogleAnalytics', 	$_GoogleAnalytics );

// settings for affiliate-tracking.php and record-sale.php
$ap_tracking = 'ap_ref_tracking'; // cookie name
$ap_sale     = 'ap_ref_sale';     // cookie name
$ap_sponsor_tracking = 'ap_sponsor_tracking';
$days_to_expiration  = 30; // set cookie for N days for $ap_tracking and $ap_sponsor_tracking
$levels = 10; // MULT-TIER COMMISSIONS max levels

// set locale var
if( isset($_SESSION['locale']) ) $locale = $_SESSION['locale'];
else $locale = 'en-US';

$device = [ // device names
	0  => 'Unknown',
	1  => 'Desktop',
	2  => 'Phone',
	3  => 'Tablet',
];

// get DB connection
$mysqli = new mysqli( AP_HOST, AP_USER, AP_PASSWORD, AP_DATABASE );
$thread_id = $mysqli->thread_id; // determine our thread id

// layout settings ----------
$cssTheme = 'theme-bm';
$pages4notLogged = [ 'index','signup','login','forgot','why','how','contact','reset','privacy-policy','terms-and-conditions' ];
$pagesNot4Logged = [ 'index','signup','login','forgot' ];
$pages4notApprd  = [ 'dashboard','contact' ];

// meta tags for each page
$META_TAGS = array( // URI => [ 'title', 'description' ]
	// 'index' => [ 'title', 'descr' ], // bypass DB settings
	'why' => [
		'Why to Choose 24Monetize?',
		'By choosing 24Monetize, you get higher conversions from your traffic within 31 countries in Africa.',
	],
	'how' => [
		'How 24Monetize Works?',
		'24Monetize is a user-friendly platform for traffic monetization. Join free, promote world biggest lotteries and get paid instantly.',
	],
	'contact' => [
		'Speak to 24Monetize Team',
		'Contact the 24monetize affiliate team. Speak to one of our monetization experts today! We’re available 24/7/365.',
	],
	'login' => [
		'Login to 24Monetize',
		'Login to 24Monetize and start promoting the biggest lotteries online.',
	],
	'forgot' => [
		'Forgot your password to 24Monetize?',
		'Forgot your password to 24Monetize? Enter you email below to reset it.',
	],
	'signup' => [
		'Register to 24monetize - Africa lottery affiliate program',
		'Fill the form and register to our lottery affiliate program - promote the most popular lotteries around the world.',
	]
);

// functions ---------------------------------------------------
function _setcookie( $c_name, $c_val, $days = 0, $path = '/' ) {
	if( php_sapi_name()=='cli' ) return '';
	return "
<script>
function af_createCookie( name, value, days, path ) {
	var expires = '; expires=Sun, 01 Jan 2017 00:00:00 GMT';
    if(days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = '; expires=' + date.toGMTString();
    }
    document.cookie = name + '=' + value + expires + '; path=' + path;
}
af_createCookie( '$c_name', '$c_val', $days, '$path' );
</script>";
}
