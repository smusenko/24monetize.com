<?php
/**
* load.php - Page Loader
*
* @author     Sergey V Musenko <info@ws123.net>
* @date       2016-12-30 15:45:14 EET
* @version    0.1
*/

$pg   = str_replace( ['.','/',' '], '', @$_GET['page'] );

#echo '-QS: '.$_SERVER['QUERY_STRING'];
#echo '<br>=lottery: '.$_GET['lottery'];
#echo '<br>=ref: '.$_GET['ref'];
#exit;
include( 'redirections.php' );

$file = "pages/$pg.php";
//echo "$pg<br>$file<br>".(int)is_file($file); exit;
if( empty($pg) || !is_file($file) )
	header( 'Location: /' );
else
	include( $file );
