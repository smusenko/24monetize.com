<?php
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<!-- YOUR CONTENT GOES HERE -->
	<div id="page-content-wrapper">
		<div class="container-fluid">

			<?php include('assets/comp/sales-stat-boxes-user.php'); ?>

			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary floatfix">
							<span class="title"><?= $lang['ADVANCED_REPORTS']; ?></span>
							<div class="pull-right no-text-shadow">
								<div id="set-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive"
									data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="fa-calendar"></i>
									<span></span>
									<i class="fa-angle-down"></i>
									<form method="post" action="data-functions/set-filter" class='date-form hidden'>
										<input type="hidden" name="redirect" value="/<?= $pg ?>">
										<input type="text" name="start_date" value="<?php echo $start_date; ?>">
										<input type="text" name="end_date" value="<?php echo $end_date; ?>">
									</form>
								</div>
							</div>
						</div>
						<div class="panel-content">
							<table id="reports-on-top-period" class="row-border" cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?= $lang['VISITORS'];?></th>
									<th><?= $lang['REGISTRATIONS'];?></th>
									<th><?= $lang['REAL_MONEY'];?></th>
									<th><?= $lang['REVENUE'];?></th>
									<th><?= $lang['COMMISSION'];?></th>
								</tr>
								</thead>
								<tbody>
								<?= referrals_reports_on_top_period( $start_date, $end_date, ($admin_user=='1' ? 0:$owner) ) ?>
								</tbody>
							</table>
						</div>
					</div>
					<!-- End Panel -->
				</div>
			</div>
		</div>
		<!-- End Page Content -->

	</div><!-- End Main Wrapper  -->

	<?php include('assets/comp/footer.php'); ?>

<script src='/assets/js/handleDaterangepicker.js'></script>
<script>
	var _startDate=new Date(<?= 1000*strtotime($start_date) ?>), _endDate=new Date(<?= 1000*strtotime($end_date) ?>)
	$(document).ready(function () {
		$('#reports-on-top-period').dataTable({ 'ordering':false, 'searching':false, 'paging':false, 'info':false });
		handleDateRangePicker('#set-report-range',_startDate,_endDate);
	})
</script>

</body>
</html>
