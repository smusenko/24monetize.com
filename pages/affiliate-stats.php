<?php
include('includes/startup.php');
$affiliate_filter = filter_input( INPUT_GET, 'a', FILTER_SANITIZE_STRING );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();
//REDIRECT ADMIN
if( !$admin_user || !$affiliate_filter ) { header( 'Location: dashboard' ); exit; }

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<div id="page-content-wrapper">
		<div class="container-fluid">
			<?php include('assets/comp/individual-stat-boxes.php'); ?>
			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary floatfix">
							<span class="title"><?= $lang['SALES_AND_PROFITS'].' ('.affiliate_name( $affiliate_filter ).')' ?></span>
							<div class="pull-right no-text-shadow">
								<div id="set-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive"
									data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="fa-calendar"></i>
									<span></span>
									<i class="fa-angle-down"></i>
									<form method="post" action="data-functions/set-filter" class='date-form hidden'>
										<input type="hidden" name="redirect" value="/<?= $pg ?>">
										<input type="text" name="start_date" value="<?php echo $start_date; ?>">
										<input type="text" name="end_date" value="<?php echo $end_date; ?>">
									</form>
								</div>
							</div>
						</div>

						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="users" class="row-border" cellspacing="0" width="100%">
									<thead>
									<tr>
										<th><?= $lang['AFFILIATE']; ?></th>
										<th>ID</th>
										<th><?= $lang['PRODUCT']; ?></th>
										<th><?= $lang['SALE_AMOUNT']; ?></th>
										<th><?= $lang['COMISSION']; ?></th>
										<th><?= $lang['NET_EARNINGS']; ?></th>
										<?php if( rc_on() ) { ?>
										<th><?= $lang['RECURRING'] ?></th>
										<?php } ?>
										<th><?= $lang['DATETIME']; ?></th>
										<th><?= $lang['ACTION']; ?></th>
									</tr>
									</thead>

									<tbody>
									<?= sales_table( $start_date, $end_date, $affiliate_filter ); ?>
									</tbody>
								</table>
							</div>
						</div>

					</div>
					<!-- End Panel -->
				</div>
			</div>
			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?= $lang['REFERRAL_TRAFFIC'].' ('.affiliate_name( $affiliate_filter ).')' ?></span>
						</div>
						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="traffic" class="row-border" cellspacing="0" width="100%">
									<thead>
									<tr>
										<th><?php echo $lang['AFFILIATE']; ?></th>
										<th><?php echo $lang['IP_ADDRESS']; ?></th>
										<th><?php echo $lang['BROWSER']; ?></th>
										<th>OS</th>
										<th><?php echo $lang['COUNTRY']; ?></th>
										<th><?= $lang['DEVICE'] ?></th>
										<th><?php echo $lang['LANDING_PAGE']; ?></th>
										<th>CPC</th>
										<th style='min-width:55px'><?php echo $lang['DATETIME']; ?></th>
										<th><?php echo $lang['ACTION']; ?></th>
									</tr>
									</thead>
									<tbody>
									<?php /* referral_table( $start_date, $end_date, $affiliate_filter ); */ ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- End Panel -->
				</div>
			</div>
		</div>
		<!-- End Page Content -->

	</div><!-- End Main Wrapper  -->

	<?php include('assets/comp/footer.php'); ?>

<script src='/assets/js/handleDaterangepicker.js'></script>
<script>
	var _startDate=new Date(<?= 1000*strtotime($start_date) ?>), _endDate=new Date(<?= 1000*strtotime($end_date) ?>)
	$(document).ready(function () {
		$('#users').DataTable({ "aaSorting": [] /* Disable initial sort */ });

		$('#traffic').DataTable({
			"aaSorting": [[8,'desc']], // def: datetime desc
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "/data-functions/ajax_referral-traffic.php?a=<?= $affiliate_filter ?>",
			"aoColumns": [{"sName":"1"},{"sName":"2"},{"sName":"3"},{"sName":"4"},{"sName":"5"},{"sName":"6"},{"sName":"7"},{"sName":"cpc"},{"sName":"datetime"},{"sName":"action","bSortable":false}]
		});


		handleDateRangePicker('#set-report-range',_startDate,_endDate);
	});

	<?php
	if( isset($_SESSION['action_deleted']) ) {
		echo 'swal("Deleted", "This has been deleted as requested!", "success")';
	}
	unset($_SESSION['action_deleted']);
	?>
</script>

</body>
</html>
