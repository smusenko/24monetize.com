<?php
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();
//REDIRECT ADMIN
if( !$admin_user ) { header( 'Location: dashboard' ); exit; }

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<div id="page-content-wrapper">
		<div class="container-fluid">
			<?php include('assets/comp/affiliates-stat-boxes.php'); ?>
			<div class="row">
				<div class="col-lg-12">
					<div class="panel"><!-- Start Panel -->
						<div class="panel-heading panel-primary">
							<span class="title"><?php echo $lang['AFFILIATES']; ?></span>
						</div>
						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="users" class="row-border" cellspacing="0" width="100%">
									<thead>
									<tr>
										<th><?= $lang['NAME']; ?></th>
										<th>ID</th>
										<th><?= $lang['USERNAME']; ?></th>
										<th><?= $lang['EMAIL']; ?></th>
										<th><?= $lang['VISITORS']; ?></th>
										<th><?= $lang['REVENUE']; ?></th>
										<th><?= $lang['BALANCE']; ?></th>
										<th><?= $lang['JOIN_DATE']; ?></th>
										<th><?= $lang['ACTION']; ?></th>
									</tr>
									</thead>
									<tbody>
									<?php /* affiliates_table(); */ ?>
									</tbody>
								</table>
							</div>
						</div>
					</div><!-- panel -->
				</div>
			</div>
		</div>
	</div><!-- End Page Content -->
</div><!-- End Main Wrapper  -->


<div id='addMoney'>
	<div class='bg'></div>
	<div class='box'>
		<div class='wrap'>
			<h2>Add Earnings</h2>
			<p><b>Affiliate:</b> <span class='aname'>Test</span>
			<form method="post" action="data-functions/affiliate-addmoney" onsubmit="return addMoneyCheck(this)">
				<label>Amount, USD</label>
				<input type='text' name='amount' class='am' value='' maxlength='6'/>
				<label>Product</label>
				<input type='text' name='product' value='' maxlength='255'/>
				<label>Internal comment</label>
				<input type='text' name='comment' value='' placeholder='optional' maxlength='255'/>
				<input type='hidden' name='a' value='0'/>
				<br clear='both'/>
				<button class='btn btn-sm btn-money submit'>Add</button>
			</form>
			<button class='btn btn-sm cancel' onclick="$('#addMoney').fadeOut();">Cancel</button>
		</div>
	</div>
</div>

<?php include('assets/comp/footer.php'); ?>

<script>
	function addMoneyForm(id, name) {
		$('#addMoney form')[0].a.value = id;
		$('#addMoney .aname').html(name + ', ID=' + id);
		$('#addMoney').fadeIn();
		$('#addMoney form')[0].amount.focus();
	}
	function addMoneyCheck(f) {
		if (f.amount.value == '' || f.amount.value == '0') f.amount.focus();
		else if (f.product.value == '' || f.product.value.length < 4) f.product.focus();
		else return true
		return false;
	}

	$(document).ready(function () {
		$('#users').DataTable({
			"aaSorting": [[7,'desc']], // def: datetime desc
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "/data-functions/ajax_affiliates-table.php",
			"aoColumns": [{"sName":"1"},{"sName":"2"},{"sName":"3"},{"sName":"4"},{"sName":"5"},{"sName":"6"},{"sName":"7"},{"sName":"8"},{"sName":"action","bSortable":false}]
		});
		$(document).keyup(function (e) {
			if (e.keyCode == 27 && $('#addMoney').css("display") == 'block') $('#addMoney').fadeOut();
		});
	});

	<?php
	if( isset($_SESSION['action_addMoney']) ) {
		echo 'swal("Add Earnings", "Earnings has been added to Affiliate!", "success")';
	}
	if( isset($_SESSION['action_deleted']) ) {
		echo 'swal("Deleted", "This has been deleted as requested!", "success")';
	}
	unset($_SESSION['action_deleted'], $_SESSION['action_addMoney']);
	?>

</script>

</body>
</html>
