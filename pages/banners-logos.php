<?php
include('includes/startup.php');
include('config/banners-logos-config.php');

//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();
$url = filter_input( INPUT_POST, 'url', FILTER_SANITIZE_STRING );

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<!-- YOUR CONTENT GOES HERE -->
	<div id="page-content-wrapper">
		<div class="container-fluid">

			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-warning">
							<span class="title"><?= $lang['MM_GENERATE_LINK']; ?></span>
						</div>
						<div class="panel-content">
							<form class="form-horizontal" method="post" action="banners-logos">
								<fieldset>

									<div class="form-group">
										<label class="col-md-2 control-label" style='padding-top:0'
											   for="lotteryselect"><?= $lang['MM_SELECT_LOTTERY']; ?></label>
										<div class="col-md-10">
											<select name="lottery" id="lotteryselect" class="selectpicker"
												onchange='reloadSizeList(this)'>
<?php
	foreach( $BANNERStypes as $type=>$v )
		echo '<option value="'.$type.'" >'.$type."</option>\n";
?>
											</select>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label" style='padding-top:0'
											   for="sizeselect"><?= $lang['MM_SELECT_SIZE']; ?></label>
										<div class="col-md-10">
											<select name="size" id="sizeselect" class="selectpicker"
												onchange='lottoSelect(this)'>
<?php
	foreach( $BANNERS as $k=>$banner )
		echo '<option value="'.$banner['link'].'" data-type="'.$banner['type'].'" data-img="'.$banner['image'].'">'.$banner['name'].'</option>'."\n";
?>
											</select>
											<span class="help-block"><br/><?= $lang['MM_INSTRUCTION'] ?></span>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label"><?= $lang['MM_TXTLINK_CODE']; ?></label>
										<div class="col-md-10">
											<textarea id='txt_code' readonly style='width:100%;'
												onfocus='this.select()'></textarea>
										</div>
									</div>

									<div class="form-group">
										<label class="col-md-2 control-label"><?= $lang['MM_IMGLINK_CODE']; ?></label>
										<div class="col-md-10 text-center">
											<textarea id='img_code' readonly style='width:100%;'
												onfocus='this.select()'></textarea>
											<a href="" id='img_link' target="_blank" title='Clik to test!'><img src='' id='img_demo' style='display:none;margin-top:5px;max-width:100%' alt='Image demo'/></a>
										</div>
									</div>

								</fieldset>
							</form>
						</div>
					</div>
				</div>
				<!-- End Panel -->
			</div>

		</div>

		<div class="pluginBox" style='isplay:none'>
			<div class="col-lg-12">
				<a href='https://wordpress.org/plugins/top-3-jackpots/' target='_blank'><img src='/data/banners/plugin-1.png' alt='WordPress Monetization Plugin' style='float:left;max-width:50%;height:auto;margin-right:20px'/></a>
				<p style='padding-top:5px;'>Download Our WordPress Monetization Plugin</p>
				<p>Feature the Top 3 jackpots on your website, blog, or mobile web.<br>
				<a href='https://wordpress.org/plugins/top-3-jackpots/' target='_blank'>https://wordpress.org/plugins/top-3-jackpots/</a></p>
			</div>
		</div>

	</div>
	<!-- End Page Content -->

</div><!-- End Main Wrapper  -->

<?php include('assets/comp/footer.php'); ?>

<script>
	var sizeSlc='#sizeselect', oType;
	// change a lottery selected
	function reloadSizeList(o) {
		var s=o.selectedIndex, l=o.options[s], t=l.text, newSel=false;
		oType=l.value;
		if(oType=='24Lottos') $('.pluginBox').fadeIn();
		else $('.pluginBox').fadeOut();
		$(sizeSlc).selectpicker('deselectAll');
		$(sizeSlc+' option').each(function(){
			if( oType==$(this).data('type') ) {
				$(this).show();
				if(!newSel) newSel=$(this).prop('selected', true);
			} else $(this).hide();
		});
		$(sizeSlc).selectpicker('refresh');
		$(sizeSlc).trigger('onchange');
	}

	// change size selector, update code block
	function lottoSelect(o) {
		var s=o.selectedIndex, l=o.options[s], t=l.text.replace(/,.*/,''), u=l.value, i=$(l).data('img');
		// ref or sponsor?
		if(u.search(/\?sponsor/)<0) u+='?ref=<?= $userid ?>'; // &username=<?= urlencode($username) ?>
		else  u+='=<?= $userid ?>';
		// text link
		var c=$('#txt_code')[0];
		if(c&&u) c.value='<a href="'+u+'" target="_blank" rel="nofollow">'+t+'</a>';
		// image link
		var c=$('#img_code')[0];
		if(c&&u) c.value='<a href="'+u+'" target="_blank" rel="nofollow"><img src="'+i+'" alt="'+t+'"/></a>';
		// img demo
		var c=$('#img_demo')[0];
		if(c) {
			c.src=i;
			c.style.display='block';
		}
		// demo link
		var c=$('#img_link')[0];
		if(c) c.href=u+'?ref=<?= $userid ?>';
	}

	// init
	$(document).ready(function () {
		var o=$('#lotteryselect')[0];
		if(o) reloadSizeList(o);
		var o=$('#sizeselect')[0];
		if(o) lottoSelect(o);
	});
</script>

<style>
.dropdown-menu.open, .dropdown-menu.inner { /* selectpicker styling */
	min-height: auto !important;
	padding: 0 !important;
}
.bootstrap-select .dropdown-toggle:focus { outline: none !important; }
</style>

</body>
</html>
