<?php
include( 'includes/startup.php' );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include( 'assets/comp/header-guest.php' );
$error_msg = '';
$msgLenMax = 512;

// send to AP_CONTACTEMAIL, see config.inc.php
if( isset($_POST['fullname'], $_POST['email']) ) {
	// Sanitize and validate the data passed in
	$fullname = ucwords( filter_input( INPUT_POST, 'fullname', FILTER_SANITIZE_STRING ) );
	$email    = filter_input( INPUT_POST, 'email', FILTER_SANITIZE_EMAIL );
	$phone    = trim( filter_input( INPUT_POST, 'phone', FILTER_SANITIZE_STRING ) );
	$message  = trim( filter_input( INPUT_POST, 'message', FILTER_SANITIZE_STRING ) );
	if( !filter_var( $email, FILTER_VALIDATE_EMAIL ) ) { // Not a valid email
		$error_msg .= '<p class="error" style="color:red;">The email address you entered is not valid.</p>';
	}
	if( strlen( $message )<7 ) { // too short
		$error_msg .= '<p class="error" style="color:red;">Your message it too short.</p>';
	}
	if( strlen( $message )>$msgLenMax ) { // too long
		$error_msg .= '<p class="error" style="color:red;">Your message it too long. Please type up to '.$msgLenMax.' characters.</p>';
	}

	if( empty($error_msg) ) {
		$subject = 'Contact form submited';
		$headers = "From: ".$email." \r\n";
		$headers .= "Reply-To: ".$email." \r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		$message = "<html><body>
<h2>Hello Admin!</h2>
<p>Our customer submited a contact form:</p>
<div style='border:1px solid #ccc;padding:10px'>
<p><b>Full Name: </b>$fullname</p>
<p><b>Email: </b><a href='mailto:$email'>$email</a></p>
<p><b>Phone: </b>$phone</p>
<p><b>Message:</b><br>$message</p>
</div><br>
<p><b>Date: </b>".date('Y-m-d H:i:s')."<br>
<b>IP address: </b>".$_SERVER['REMOTE_ADDR']."</p>
<p><b>Have a nice day!</b><br><a href='http://".$DOMAIN."'>".$DOMAIN."</p>
</body></html>"; // echo "<textarea cols=100 rows=5>$message</textarea>";
		mail( AP_CONTACTEMAIL, $subject, $message, $headers );

		$error_msg = '<div style="color:green;"><br><big>
		Your message has been sent!<br>Thank you!
		</big></div><style>.control-group{display:none}</style>';
	} else {
		$error_msg = '<div><br><big>'.$error_msg.'</big></div>';
	}
}

?>
<style>
@media(min-height:736px) {
	.footer-guest {
		position: fixed;
		bottom: 0;
	}
	body { padding-bottom: 40px; }
}
</style>
<body>
<div class='bodybg'>

	<?php include('assets/comp/top-nav-guest.php'); ?>

	<!-- Page Content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-3"></div>
			<div class="col-lg-6">
				<form method="post" class="form-horizontal login-form">
					<fieldset>
						<div class="col-lg-12 center">
							<h1>Get In Touch</h1>
							<p>We try to respond as quickly as possible, but in some cases, it may take up to 48 business hours to receive an answer.</p>
							<img src='/assets/img/sep-black.png' class='sep' alt='Get In Touch'/></a>
							<?= $error_msg ?>
						</div>


						<div class="control-group">
							<label class="control-label" for="textinput1">Full Name</label>
							<div class="controls">
								<input id="textinput1" name="fullname" type="text" placeholder="Full Name" maxlength='50'
									   class="input-xlarge" value="<?= @$_POST['fullname']; ?>"
									   required="required">
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="textinput2">E-Mail Address</label>
							<div class="controls">
								<input id="textinput2" name="email" type="email" placeholder="email@provider.com" maxlength='96'
									   class="input-xlarge" value="<?= @$_POST['email']; ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="textinput3">Phone Number</label>
							<div class="controls">
								<input id="textinput3" name="phone" type="tel" pattern='+[0-9]{1-3} [0-9]{2-4} [0-9\-]{6-8}' placeholder="+123 123 123-4567" maxlength='17'
									   class="input-xlarge" value="<?= @$_POST['phone']; ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="textinput4">Your Message</label>
							<div class="controls">
								<textarea id="textinput4" name="message" class="text-xlarge" rows='3' required
									placeholder='Up to <?= $msgLenMax ?> characters please!'><?= @$_POST['message']; ?></textarea>
							</div>
						</div>

						<div class="control-group">
							<div class="controls">
								<input type="submit" class="btn btn-warning btn-block register-btn" value="Send">
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

</div>

<?php include('assets/comp/footer-guest.php'); ?>

</body>
</html>
