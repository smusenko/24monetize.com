<?php
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->

<?php include('assets/comp/top-nav.php'); ?>

<div id="wrapper">
	<?php include('assets/comp/side-nav.php'); ?>
	<div id="page-content-wrapper">
		<div class="container-fluid">

<?php if( $user_status!=1 ) { // NOT APPROVED YET USER
	include_once( $_SERVER['DOCUMENT_ROOT'].'/assets/comp/emailtemplates.php'); ?>
		<div class="row" style='padding:20px;font-size:120%;'>
<h1>Thank you!</h1>
<p>Thank you for signing up for the <a href='<?= DOMAINURL ?>'><?= AP_BRAND ?></a> affiliate program!</p>
<p><b>Please Note</b>: For your account to be approved, we would appreciate it if you
send an email to <a href='mailto:<?= AP_CONTACTEMAIL ?>'><?= AP_CONTACTEMAIL ?></a> with a brief summary of how you plan to promote
and generate traffic to <a href='https://24lottos.com'>24Lottos</a>.
<p><b>Good Luck!</b></p>
<p>Thanks in advance,</p>
<?= $EMAILSIGN ?>
		</div>
	<?php include('assets/comp/footer.php'); ?>


<?php } else { /* approved, admin/user */ ?>

			<?php
			if( $admin_user == '1' ) {
				include('assets/comp/stat-boxes.php');
			} else {
				include('assets/comp/stat-boxes-user.php');
			} ?>

			<div class="row">
<?php if( $admin_user ) { /* Top Affilliates - admin mode */?>
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?= $admin_user=='1' ? $lang['TOP_AFFILIATES'] : $lang['RECENT_TRAFFIC'].' <span class="small">('.$DOMAIN.'?ref='.$owner.')</span>' ?></span>
						</div>
						<div class="panel-content">
							<div id="status"></div>
							<table id="top-affiliates" class="row-border" cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?= $lang['USERNAME']; ?></th>
									<th><?= $lang['VISITORS']; ?></th>
									<th><?= $lang['REGISTRATIONS']; ?></th>
									<th><?= $lang['REAL_MONEY']; ?></th>
									<th><?= $lang['CONVERSION_RATE']; ?></th>
									<th><?= $lang['REVENUE']; ?></th>
									<th><?= $lang['COMMISSION']; ?></th>
								</tr>
								</thead>
								<tbody>
								<?= top_affiliates_table('regs'); ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End Panel -->

<?php } else { /* Quick reports - user mode */?>
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?= $lang['REPORTS_ON_TOP']; ?></span>
						</div>
						<div class="panel-content">
							<table id="reports-on-top" class="row-border" cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?= $lang['PERIOD'];?></th>
									<th><?= $lang['VISITORS'];?></th>
									<th><?= $lang['REGISTRATIONS']; ?></th>
									<th><?= $lang['REAL_MONEY'];?></th>
									<th><?= $lang['REVENUE'];?></th>
									<th><?= $lang['COMMISSION'];?></th>
								</tr>
								</thead>
								<tbody>
								<?= referrals_reports_on_top( $owner ) ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End Panel -->
<?php } ?>

			</div>

			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-4">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?= $lang['VISITORS'] /* 'REFERRAL_TRAFFIC' */ ?></span>
						</div>
						<div class="panel-content">
							<label for="recent_traffic" style="width:100%;">Recent Traffic History<br/>
								<canvas id="recent_traffic" style="width:100%;" height="205"></canvas>
							</label>
						</div>
					</div>
				</div>
				<!-- End Panel -->

				<!-- Start Panel -->
				<div class="col-lg-4">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?= $lang['TOP_COUNTRY']; ?></span>
						</div>
						<div class="panel-content">
							<ul class="top-countries">
								<?= top_country_ts_table( $admin_user>0 ? 0 : $owner); ?>
							</ul>
						</div>
					</div>
				</div>
				<!-- End Panel -->

				<!-- Start Panel -->
				<div class="col-lg-4">
					<div class="panel">
						<div class="panel-heading panel-primary _warning">
							<span class="title"><?= $lang['DEVICE_STATISTICS']; ?></span>
							<div class="pull-right mr">
								<a href="<?php if( $admin_user == '1' ) {
									echo 'referral-traffic';
								} else {
									echo 'my-sales';
								} ?>" class="btn btn-sm btn-primary"><?= $lang['VIEW_ALL_TRAFFIC']; ?></a>
							</div>
						</div>
						<div class="panel-content">
							<div class="row">
								<div class="col-lg-4">
									<div class="desktop-legend"></div> <?= $lang['DESKTOP']; ?><br>
									<div class="phone-legend"></div> <?= $lang['PHONE']; ?> <br>
									<div class="tablet-legend"></div> <?= $lang['TABLET']; ?>
								</div>
								<div class="col-lg-8">
									<canvas id="pa" style="width:100%;" height="190"></canvas>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- End Panel -->
			</div>

<?php /* ?>
			<div class="row">

				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary _warning">
							<span class="title"><?= $lang['MONTHLY_REFERRALS_SALES']; ?></span>
							<div class="pull-right mr">
								<a href="<?= $admin_user ? 'referral-traffic':'my-traffic'; ?>" class="btn btn-sm btn-primary"><?= $lang['VIEW_ALL_TRAFFIC']; ?></a>
								<a href="<?= $admin_user ? 'sales-profits':'my-sales' ?>" class="btn btn-sm btn-warning"><?= $lang['VIEW_ALL_SALES']; ?></a>
							</div>
						</div>
						<div class="panel-content">
							<div style="width:99%;">
								<canvas id="lc" height="77"></canvas>
							</div>
						</div>
					</div>
				</div>
				<!-- End Panel -->

			</div>

			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-warning">
							<span class="title"><?= $lang['RECENT_SALES']; ?></span>
							<div class="pull-right mr">
								<a href="<?php if( $admin_user == '1' ) {
									echo 'sales-profits';
								} else {
									echo 'my-sales';
								} ?>" class="btn btn-sm btn-warning">View all Sales</a>
							</div>
						</div>
						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="sales" class="row-border" cellspacing="0" width="100%">
									<thead>
									<tr>
										<?php if( $admin_user == '1' ) { ?>
											<th><?= $lang['AFFILIATE']; ?></th><?php } ?>
										<th><?= $lang['PRODUCT']; ?></th>
										<th><?= $lang['SALE_AMOUNT']; ?></th>
										<th><?= $lang['COMISSION']; ?></th>
										<th><?= $lang['NET_EARNINGS']; ?></th>
										<?php $rc_on = rc_on();
										if( $rc_on == '1' ) {
											echo '<th>'.$lang['RECURRING'].'</th>';
										} ?>
										<th><?= $lang['DATETIME']; ?></th>
									</tr>
									</thead>

									<tbody>
									<?php if( $admin_user == '1' ) {
										recent_sales_table();
									} else {
										my_recent_sales_table( $owner );
									} ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<!-- End Panel -->
			</div>

<?php */ ?>

<?php include('assets/comp/footer.php'); ?>

<script>
	$(document).ready(function () {
		$('#reports-on-top').dataTable({ 'ordering':false, 'searching':false, 'paging':false, 'info':false });
		$('#top-affiliates').dataTable({ "aaSorting": [], 'searching':false, 'paging':false, 'info':false });
		//$('#sales').dataTable({ "aaSorting": [] /* no initial sort */ });
	})

	<?php // VISITORS stats
		$visits = bar_chart_unqiue_visits( $admin_user ? 0:$owner );
		$vis_dates = array_keys($visits);
		$vis_vals = array_values($visits); ?>
	var barData = {
		labels: ['<?= implode("','",$vis_dates) ?>'],
		datasets: [ {
			fillColor: "#E6EEF7",
			strokeColor: "#FF4D00",
			data: ['<?= implode("','",$vis_vals) ?>']
		}]
	}
	var recent_traffic = document.getElementById("recent_traffic").getContext("2d");
	new Chart(recent_traffic).Bar(barData);

	var pa_data = [ // DEVICE stats
		{
			value: <?= total_device( '1', $admin_user ? 0:$owner ) ?>,
			color: "#FF4D00",
			highlight: "#FF4D00",
			label: "<?= $lang['DESKTOP'];?>"
		},{
			value: <?= total_device( '2', $admin_user ? 0:$owner ) ?>,
			color: "#0A5AA1",
			highlight: "#0A5AA1",
			label: "<?= $lang['PHONE'];?>"
		},{
			value: <?= total_device( '3', $admin_user ? 0:$owner ) ?>,
			color: "#333",
			highlight: "#333",
			label: "<?= $lang['TABLET'];?>"
		}
	];
	var pa_options = {
		responsive: true,
		maintainAspectRatio: true
	}
	var pa = document.getElementById("pa").getContext("2d");
	new Chart(pa).Doughnut(pa_data, pa_options);

<?php /* ?>
	var lc_data = {
		<?= referrals_sales_line_chart( $admin_user ? 0:$owner );?>
	};
	var lc_options = {
		pointDotStrokeWidth: 4,
		pointDotRadius: 8,
		bezierCurve: false,
		datasetStrokeWidth: 7,
		datasetFill: false,
		multiTooltipTemplate: "<%= value %> - <%= datasetLabel %>",
		responsive: true,
		maintainAspectRatio: true
	};
	var lc = document.getElementById("lc").getContext("2d");
	new Chart(lc).Line(lc_data, lc_options);
<?php */ ?>

</script>


<?php } ?>

		</div><!-- End container-fluid -->
	</div><!-- End page-content-wrapper -->
</div><!-- End wrapper  -->

<?php if( isset($_SESSION['loginStatus']) && $_SESSION['loginStatus']=='signup' ) { ?>
	<!-- Google Code for Google Conversion Pixel Conversion Page --> <script type="text/javascript">
	/* <![CDATA[ */
	var google_conversion_id = 869685886;
	var google_conversion_language = "en";
	var google_conversion_format = "3";
	var google_conversion_color = "ffffff";
	var google_conversion_label = "zbW-COrcxHAQ_rTZngM"; var google_remarketing_only = false;
	/* ]]> */
	</script>
	<script type="text/javascript"  
	src="//www.googleadservices.com/pagead/conversion.js">
	</script>
	<noscript><div style="display:inline;"><img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/869685886/?label=zbW-COrcxHAQ_rTZngM&amp;guid=ON&amp;script=0"/></div></noscript>
	<?php unset($_SESSION['loginStatus']);
} ?>

</body>
</html>
