<?php
// external Aff, 2020-01-13, Sergey
header("HTTP/1.1 301");
header( "location: /" );
exit;


include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include('assets/comp/header-guest.php');
?>

<body>

<!-- Page Content -->
<div class="container">

	<div class="row">
		<div class="col-sm-12 col-md-6 col-md-offset-3">

			<div style='padding:30px'><a href='/'><?= site_logo(220); ?></a></div>
			<form method="post" action="data-functions/forgot" class="form-horizontal login-form">
				<fieldset>
					<?php if( @$_GET['success'] == '1' ) {
						echo '<span class="success-text">Reset instructions have been sent to your email</span>';
					}
					if( @$_GET['error'] == '1' ) {
						echo '<span class="red">We are sorry we are unable to locate your account information.</span>';
					} ?>
					<h4>
						Forgot your password? Enter you email below to reset it.
					</h4>
					<!-- Username -->
					<div class="control-group">
						<label class="control-label" for="textinput">E-mail Address</label>
						<div class="controls">
							<input id="textinput" name="se" type="text" placeholder="me@myemail.com"
								   class="input-xlarge">
						</div>
					</div>

					<div class="control-group forgot-link">
						<a href="https://affiliates.24monetize.com" data-old="login">Figured it out? Go to the login form</a>
					</div>

					<!-- Submit-->
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-primary btn-block login-btn" value="Send Email">
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<!-- /.container -->

<!-- jQuery -->
<script src="assets/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="assets/js/bootstrap.min.js"></script>

</body>
</html>
