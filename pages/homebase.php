<?php
include( 'includes/startup.php' );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include( 'assets/comp/header-guest.php' );
?>

<body>
<div class='bodybg'>

	<?php include('assets/comp/top-nav-guest.php'); ?>

	<!-- Page Content -->
	<div class="index-center-banner">
		<div class="container">
			<div class="row">
				<h2>Promote the World’s Biggest Lottery Brands</h2>
				<img src='/assets/img/sep-white.png' class='sep' alt='World’s Biggest Lottery Brands'/></a>
				<h3>Join 24Monetize and profit from Africa like never before.</h3>
				<a href="https://affiliates.24monetize.com/signup" data-old="/signup"><img src='/assets/img/btn-join-big.png' alt='Join Now'/></a>
			</div>
		</div>
	</div>
    <!-- Page Content -->
    <div class="container">
        <div class="row center nopadding">
            <div class="center">
                <h1>How Does it Work ?</h1>
                <img src='/assets/img/sep-green.png' class='sep' alt='How Does it Work'/></a>
            </div>
            <div class='how-wrap'>
                <div>
                    <div class='w1'><img src='/assets/img/how1.png' alt='Register'/></div>
                    <div class='t'><h3>Register</h3>Apply for an Account with
                        <a href="https://affiliates.24monetize.com/signup" data-old="/signup"><u>24Monetize</u></a></div>
                </div>
                <div>
                    <div class='w2'><img src='/assets/img/how2.png' alt='Advertise'/></div>
                    <div class='t'><h3>Advertise</h3>Advertise using our wide range of lotteries using our deeplink generator.</div>
                </div>
                <div>
                    <div class='w3'><img src='/assets/img/how3.png' alt='Get Paid'/></div>
                    <div class='t'><h3>Get Paid</h3>Earn a commission up to 15% on all orders your customers make <b><u>forever</u></b> from clicking on your ad.</div>
                </div>
            </div>
        </div>
        <div align='center'>
            <a href="https://affiliates.24monetize.com/signup" data-old="/signup"><img src='/assets/img/btn-join-big.png' alt='Join Now'/></a>
        </div>
        &nbsp;
    </div>
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="lotto-wrap center">
<div class='l-logo'><img src='/data/lottos/powerball.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/ozlotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/francelotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/malawinl.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/ghanalotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/megamillions.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/ugandalotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/laprimitiva.png' alt=''/></div>
<?php /*
<div class='l-logo'><img src='/data/lottos/canada6-49.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/elgordo.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/euromillions.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/germanylotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/irishlotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/kenya-lotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/marrocolotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/megasena.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/nigerialotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/superenalotto.png' alt=''/></div>
*/ ?>
				</div>
			</div>
		</div>
	</div>


</div>

<?php include('assets/comp/footer-guest.php'); ?>

</body>
</html>
