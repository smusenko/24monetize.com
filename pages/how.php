<?php
include( 'includes/startup.php' );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include( 'assets/comp/header-guest.php' );
?>
<style>
@media(min-height:840px) {
	.footer-guest {
		position: fixed;
		bottom: 0;
	}
	body { padding-bottom: 40px; }
}
</style>

<body>
<div class='bodybg'>

	<?php include('assets/comp/top-nav-guest.php'); ?>

	<div class="container">
		<div class="row center nopadding">
			<div class="center">
				<h1>How Does it Work ?</h1>
				<img src='/assets/img/sep-green.png' class='sep' alt='How Does it Work'/></a>
			</div>
			<div class='how-wrap'>
				<div>
					<div class='w1'><img src='/assets/img/how1.png' alt='Register'/></div>
					<div class='t'><h3>Register</h3>Apply for an Account with
						<a href="https://affiliates.24monetize.com/signup"><u>24Monetize</u></a></div>
				</div>
				<div>
					<div class='w2'><img src='/assets/img/how2.png' alt='Advertise'/></div>
					<div class='t'><h3>Advertise</h3>Advertise using our wide range of lotteries using our deeplink generator.</div>
				</div>
				<div>
					<div class='w3'><img src='/assets/img/how3.png' alt='Get Paid'/></div>
					<div class='t'><h3>Get Paid</h3>Earn a commission on all orders your customers make <b><u>forever</u></b> from clicking on your ad.</div>
				</div>
			</div>
		</div>
		<div align='center'>
			<input type="button" class="btn btn-warning btn-block register-btn" value="Join Now"
				onclick='location.href="https://affiliates.24monetize.com/signup"'>
			&nbsp;
		</div>
		&nbsp;
	</div>

</div>

<?php include('assets/comp/footer-guest.php'); ?>

</body>
</html>
