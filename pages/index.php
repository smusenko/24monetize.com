<?php
include( 'includes/startup.php' );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include( 'assets/comp/header-guest.php' );
?>

<body>
<div class='bodybg'>

	<?php include('assets/comp/top-nav-guest.php'); ?>

	<!-- Page Content -->
	<div class="index-center-banner">
		<div class="container">
			<div class="row">
				<h1>24Lottos Affiliate Program</h1>
				<img src='/assets/img/sep-white.png' class='sep' alt='World’s Biggest Lottery Brands'/></a>
				<h2>Earn up to 30% recurring commission for 24Lottos sales, forever!</h2>
				<br>
				<input type="button" class="btn btn-warning btn-block register-btn" value="Join Now"
					onclick='location.href="https://affiliates.24monetize.com/signup"'>
				&nbsp;
			</div>
		</div>
	</div>

	<section class="section-white">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="lotto-wrap center">
						<div class='l-logo'><img src='/data/lottos/powerball.png' alt=''/></div>
						<div class='l-logo'><img src='/data/lottos/ozlotto.png' alt=''/></div>
						<div class='l-logo'><img src='/data/lottos/francelotto.png' alt=''/></div>
						<div class='l-logo'><img src='/data/lottos/malawinl.png' alt=''/></div>
						<div class='l-logo'><img src='/data/lottos/ghanalotto.png' alt=''/></div>
						<div class='l-logo'><img src='/data/lottos/megamillions.png' alt=''/></div>
						<div class='l-logo'><img src='/data/lottos/ugandalotto.png' alt=''/></div>
						<div class='l-logo'><img src='/data/lottos/laprimitiva.png' alt=''/></div>
						<?php /*
<div class='l-logo'><img src='/data/lottos/canada6-49.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/elgordo.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/euromillions.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/germanylotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/irishlotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/kenya-lotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/marrocolotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/megasena.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/nigerialotto.png' alt=''/></div>
<div class='l-logo'><img src='/data/lottos/superenalotto.png' alt=''/></div>
*/ ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="section-gray">
		<div class="container">
			<div class="row nopadding">
				<div class="center-block">
					<div class="center">
						<h3>Our Advantage. Our Intelligence.</h3>
						<img src='/assets/img/sep-green.png' class='sep' alt='How Does it Work'/></a>
						<br>&nbsp;
					</div>
					<div class="row margin-10">
						<div class='col-lg-4 col-md-4 col-sm-4 col-lg-offset-2 col-md-offset-2 text-center'><img src='/assets/img/high_conversion.png' alt='Highest conversion'/></div>
						<div class='col-lg-5 col-md-4 col-sm-7'>
							<span class="medium-title">Payments, Payments, Payments</span>
							<p class="medium-text">Great conversion rates, retention rates and high player value make 24Monetize special! Take advantage of our extensive range of modern marketing tools to turn clicks into cash.</p>
						</div>
					</div>
					<div class="row margin-10">
						<div class='col-lg-4 col-md-4 col-sm-4 col-lg-offset-2 col-md-offset-2 col-lg-push-4 col-md-push-4 col-sm-push-7 text-center'><img src='/assets/img/high_payouts.png' alt='Highest payouts'/></div>
						<div class='col-lg-4 col-md-4 col-sm-7 col-lg-pull-4 col-md-pull-4 col-sm-pull-4'>
							<span class="medium-title">Increase your commission, today!</span>
							<p class="medium-text">Thanks to our predictive marketing technology players are loving 24Lottos and staying longer. </p>
						</div>
					</div>
					<div class="row margin-10">
						<div class='col-lg-4 col-md-4 col-sm-4 col-lg-offset-2 col-md-offset-2 text-center'><img src='/assets/img/Industry.png' alt='Get Paid'/></div>
						<div class='col-lg-4 col-md-4 col-sm-7'>
							<span class="medium-title">Forever and ever</span>
							<p class="medium-text">Even if you stop referring new members, you continue to earn on every purchase from members you have already referred. Forever!</p>
						</div>
					</div>
					<div class="row margin-10">
						<div class='col-lg-4 col-md-4 col-sm-4 col-lg-offset-2 col-md-offset-2 col-lg-push-4 col-md-push-4 col-sm-push-7 text-center'><img src='/assets/img/24_tracking.png' alt='Highest payouts'/></div>
						<div class='col-lg-4 col-md-4 col-sm-7 col-lg-pull-4 col-md-pull-4 col-sm-pull-4'>
							<span class="medium-title">Simple as 1,2,3</span>
							<p class="medium-text">Login to your dashboard and track reports and visual graphs of your sales, traffic, account balance and see how your campaign is performing in real-time.</p>
						</div>
					</div>
					<div class="row margin-10">
						<div class='col-lg-4 col-md-4 col-sm-5 col-lg-offset-2 col-md-offset-2 text-center'><img src='/assets/img/support24-5.png' alt='Get Paid'/></div>
						<div class='col-lg-4 col-md-4 col-sm-7'>
						<span class="medium-title">We&#39;re here for you</span>
						<p class="medium-text">We&#39;re here 24/7 to help you and guide you through the process of making profit with 24Monetize.<br>
							We automatically transfer your earnings to your preferred online payment.</p>
					</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<!--section class="section-white">
		<div class="container">
			<div class="row nopadding">
				<div class="center">
					<span class="big-span">Start In 3 Easy Steps</span>
					<img src='/assets/img/sep-green.png' class='sep' alt='How Does it Work'/></a>
				</div>
				<div class="col-lg-3 col-lg-offset-2 col-sm-4  col-sm-offset-1 margin-50">
					<img src="/assets/img/easy-money.png" class="img-responsive img-circle" alt="Start Making Money In 3 Easy Steps">
				</div>
				<div class="col-lg-6 col-sm-7 margin-50">
					<ul class="features-list">
						<li><span>1</span><p>Sign up for the 24Monetize affiliate program.</p></li>
						<li><span>2</span><p>Promote any of the lottery brands using your customized link.</p></li>
						<li><span>3</span><p>Get paid.</p></li>
					</ul>
				</div>
			</div>
		</div>
	</section-->

	<section class="section-white">
		<div class="container">
			<div class="row">
				<div class="center">
					<span class="big-span">Partner With Success</span>
					<img src='/assets/img/sep-green.png' class='sep' alt='How Does it Work'/></a>
					<br>&nbsp;
				</div>
				<div class="row nopadding margin-50">
					<div class='col-sm-4 text-center'>
						<img src='/assets/img/badge1.png' alt='Highest conversion' style="border-radius: 50px;"/><br><br>
						The fastest growing lottery platform in Africa.<br>&nbsp;
					</div>
					<div class='col-sm-4 text-center'>
						<img src='/assets/img/badge2.png' alt='The simplest platform to play the lottery' style="border-radius: 50px;"/><br><br>
						A simple and fun platform to play the lottery.<br>&nbsp;
					</div>
					<div class='col-sm-4 text-center'>
						<img src='/assets/img/badge3.png' alt='12 local payments in Africa'/ style="border-radius: 50px;"><br><br>
						Powerfull payments technology and variety.<br>&nbsp;
					</div>
				</div>
			</div>
			<div class="row center">
				<input type="button" class="btn btn-warning btn-block register-btn" value="Join Now"
					onclick='location.href="https://affiliates.24monetize.com/signup"'>
			</div>
		</div>
	</section>

	<!--<section class="section-lightpurple">
		<div class="container">
			  <div class="row center">
				  <div class="center">
					  <h1>Still have questions?</h1>
					  <img src='/assets/img/sep-black.png' class='sep' alt='Get your commission fast!'/>
				  </div>
				  <div class="center margin-50">
					  <h3 class="medium-title">Don&#39;t hesitate to <a href="/contact">contact us</a>  we are here for you.</h3>
				  </div>
			  </div>
		  </div>
	</section>-->

	<section class="section-gray">
		<!-- Page Content -->
		<div class="container">
			<div class="row center">
				<div class="center">
					<span class="big-span">Get your commission fast!</span>
					<img src='/assets/img/sep-black.png' class='sep' alt='Get your commission fast!'/>
				</div>
				<div class="center margin-50">
					<div class='col-lg-2 col-md-3 col-lg-offset-3 col-md-offset-1 margin-10'><img src='/assets/img/PayPal.png' alt='PayPal logo'/></div>
					<div class='col-lg-2 col-md-3 margin-10'><img src='/assets/img/BankTransfer.png' alt='Bank Transfer logo'/></div>
					<div class='col-lg-2 col-md-3 margin-10'><img src='/assets/img/skrill.png' alt='Skrill logo'/></div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php include('assets/comp/footer-guest.php'); ?>

</body>
</html>
