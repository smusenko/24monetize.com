<?php
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<div id="page-content-wrapper">
		<div class="container-fluid">
			<?php if( $admin_user == '1' ) {
				include('assets/comp/leads-stat-boxes.php');
			} else {
				include('assets/comp/leads-stat-boxes-user.php');
			} ?>
			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?php echo $lang['LEADS']; ?></span>
						</div>
						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="users" class="row-border" cellspacing="0" width="100%">
									<?php if( $admin_user == '1' ) { /* ADMIN mode */ ?>
										<thead>
										<tr>
											<th><?= $lang['AFFILIATE']; ?></th>
											<th>ID</th>
											<th><?= $lang['NAME']; ?></th>
											<th><?= $lang['EMAIL']; ?></th>
											<th><?= $lang['PHONE']; ?></th>
											<th><?= $lang['COUNTRY']; ?></th>
											<th><?= $lang['STATUS']; ?></th>
											<th><?= $lang['MESSAGE']; ?></th>
											<th><?= $lang['NET_EARNINGS']; ?></th>
											<th><?= $lang['CONVERSION']; ?></th>
											<th><?= $lang['DATETIME']; ?></th>
											<th><?= $lang['ACTION']; ?></th>
										</tr>
										</thead>
										<tbody>
										<?php leads_table(); ?>
										</tbody>
									<?php } else { /* USER mode */ ?>
										<thead>
										<tr>
											<th><?= $lang['LEAD_ID']; ?></th>
											<th><?= $lang['NET_EARNINGS']; ?></th>
											<th><?= $lang['CONVERSION']; ?></th>
											<th><?= $lang['DATETIME']; ?></th>
										</tr>
										</thead>
										<tbody>
										<?php my_leads_table( $owner ); ?>
										</tbody>
									<?php } ?>
								</table>
							</div>
						</div>
					</div>
					<!-- End Panel -->
				</div>
			</div>
		</div>
		<!-- End Page Content -->

	</div><!-- End Main Wrapper  -->

	<?php include('assets/comp/footer.php'); ?>

	<script>
		$(document).ready(function () {
			$('#users').DataTable();
		});

		<?php
		if( isset($_SESSION['action_deleted']) ) {
			echo 'swal("Deleted", "This has been deleted as requested!", "success")';
		}
		unset($_SESSION['action_deleted']);
		?>
	</script>

</body>
</html>
