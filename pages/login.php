<?php
// external Aff, 2020-01-13, Sergey
header("HTTP/1.1 301");
header( "location: https://affiliates.24monetize.com" );
exit;


include('includes/startup.php');
include('includes/register.inc.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include('assets/comp/header-guest.php');
?>

<body>
<!-- Page Content -->
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-md-offset-3">
			<div style='padding:30px'><a href='/'><?= site_logo(220); ?></a></div>
			<form method="post" action="data-functions/process_login" class="form-horizontal login-form">
				<fieldset>
					<?php if( @$_GET['logoff'] == '1' ) {
						echo '<span class="success-text">You have logged off successfully</span>';
					}
					if( @$_GET['success'] == '1' ) {
						echo '<span class="success-text">Your password has been reset successfully.</span>';
					}
					if( @$_GET['error'] == '1' ) {
						echo '<span class="red">Invalid Username or Password</span>';
					} ?>
					<!-- Username -->
					<div class="control-group">
						<label class="control-label" for="textinput">Username or E-mail Address</label>
						<div class="controls">
							<input id="textinput" name="email" type="text" placeholder="username" class="input-xlarge">
						</div>
					</div>

					<!-- Password input-->
					<div class="control-group">
						<label class="control-label" for="passwordinput">Password</label>
						<div class="controls">
							<input id="passwordinput" name="p" type="password" placeholder="Password"
								   class="input-xlarge">
						</div>
					</div>

					<div class="control-group forgot-link">
						<a href="forgot">Forgot your username or password?</a>
					</div>

					<!-- Submit-->
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-primary btn-block login-btn" value="Login">
						</div>
					</div>

					<div class="signup">
						<span> <a href="/signup">Don't have an account?</a> </span>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<!-- /.container -->

<!-- jQuery -->
<script src="assets/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="assets/js/bootstrap.min.js"></script>

</body>
</html>
