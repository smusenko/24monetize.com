<?php
include( 'includes/startup.php' );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include( 'assets/comp/header-guest.php' );
?>

<style type="text/css">ol{margin:0;padding:0}table td,table th{padding:0}.c1{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:justify;height:11pt}.c0{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:"Arial";font-style:normal}.c3{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:left}.c2{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:justify}.c7{font-weight:400;vertical-align:baseline;font-size:11pt;font-family:"Arial";font-style:normal}.c6{text-decoration-skip-ink:none;-webkit-text-decoration-skip:none;color:#1155cc;text-decoration:underline}.c5{background-color:#ffffff;padding:72pt 72pt 72pt 72pt}.c9{color:#0000ff;text-decoration:none}.c8{color:inherit;text-decoration:inherit}.c4{height:11pt}.title{padding-top:0pt;color:#000000;font-size:26pt;padding-bottom:3pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:0pt;color:#666666;font-size:15pt;padding-bottom:16pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Arial"}p{margin:0;color:#000000;font-size:11pt;font-family:"Arial"}h1{padding-top:20pt;color:#000000;font-size:20pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-size:16pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:16pt;color:#434343;font-size:14pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:14pt;color:#666666;font-size:12pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}</style>
<body>
<div class='bodybg'>

	<?php include('assets/comp/top-nav-guest.php'); ?>

	<!-- Page Content -->
    <section class="section-white" style='margin-top:-30px'>
        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row nopadding">
                <div class="center">
                    <span class="big-span">Privacy Policy</span>
                </div>
                <div class="col-lg-12">
<br>

<p class="c3">
<span class="c0">PURPOSE OF THIS PRIVACY NOTICE</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span>This is the privacy notice (&quot;Privacy Notice&quot;) that governs how we ie 24Monetize, use Personal Information (defined below) that we collect, receive and store about individuals who visit or use the </span>
<span class="c6">
<a class="c8" href="https://www.google.com/url?q=http://www.24lottos.com/&amp;sa=D&amp;ust=1579869724103000">www.24</a>
</span>
<span class="c6">Monetize.com</span>
<span class="c0">&nbsp;(together with its sub-domains, content and services, the &quot;Site&quot;). This Privacy Notice forms part of the Terms of Use, available on the website (the &quot;Terms&quot;).</span>
</p>
<p class="c2">
<span class="c0">The Site is not structured to attract individuals under the age of 18. Accordingly, we do not intend to collect Personal Information from anyone we know to be under 18 years.</span>
</p>
<p class="c2">
<span class="c0">It is important that you read this privacy notice together with any other privacy notice or fair processing notice we may provide on specific occasions when we are collecting or processing personal data about you so that you are fully aware of how and why we are using your data. This privacy notice supplements the other notices and is not intended to override them.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">INTRODUCTION</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Personal Information. We have implemented a Privacy Policy because your privacy, and the privacy of other Site users, is important to us. We provide this Privacy Notice explaining our online information practices and the choices you can make about the way your Personal Information is collected and used in connection with the Site and certain services that we offer through or in connection with the Site. &ldquo;Personal Information&rdquo; means any information that may be used, either alone or in combination with other information, to personally identify an individual, including, but not limited to, a first and last name, a personal profile, an email address, a home or other physical address, or other contact information. It does not include data where the identity has been removed (anonymous data).</span>
</p>
<p class="c2">
<span class="c0">English and Translated Versions. If you are reading a translation of this Privacy Notice in a language other than English, you acknowledge and agree that (i) the English version is the official version; (ii) the non-English version is provided for your convenience only and the translation will not be valid as an agreement; and (iii) in the event of any inconsistency between the English and a non-English version, the English version will prevail and govern.</span>
</p>
<p class="c2">
<span class="c0">Consent and Modification. By using the Site, you consent to the terms of this Privacy Notice and to the processing of Personal Information for the purposes set forth herein. If you do not agree to this Privacy Notice, please do not use the Site. We reserve the right, at our discretion, to change the Privacy Notice at any time; if we do so we will notify in advance of such changes. The changes will come into effect following your confirmation of acceptance thereof. This version was last updated on 10/01/2020 and historic versions can be obtained by contacting us.</span>
</p>
<p class="c2">
<span class="c0">Personal Data Changes. It is important that the personal data we hold about you is accurate and current. Please keep us informed if your personal data changes during your relationship with us.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">CONTROLLER</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c6">
<a class="c8" href="https://www.google.com/url?q=http://www.24lottos.com/&amp;sa=D&amp;ust=1579869724104000">www.24</a>
</span>
<span class="c6">Monetize.com</span>
<span class="c0">, referred to as the Company in this privacy notice, is the controller and responsible for your personal data.</span>
</p>
<p class="c2">
<span class="c0">We have appointed a data protection officer (DPO) who is responsible for overseeing questions in relation to this privacy notice. If you have any questions about this privacy notice, including any requests to exercise your legal rights, please contact the DPO using the details set out below.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">CONTACT DETAILS</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">Our full details are:</span>
</p>
<p class="c3">
<span class="c0">Full name of controller: 24Monetize</span>
</p>
<p class="c3">
<span>Email address:info@</span>
<span class="c6">
<a class="c8" href="https://www.google.com/url?q=http://www.24lottos.com/&amp;sa=D&amp;ust=1579869724105000">.24</a>
</span>
<span class="c6 c7">Monetize.com</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">This website may include links to third-party websites, plug-ins and applications. Clicking on those links or enabling those connections may allow third parties to collect or share data about you.We do not control these third-party websites and are not responsible for their privacy statements. When you leave our website, we encourage you to read the privacy notice of every website you visit.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">2. THE DATA WE COLLECT ABOUT YOU</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We do not currently require you to provide Personal Information in order to have access to general information available on the Site.</span>
</p>
<p class="c2">
<span class="c0">We may however, collect, use, store and transfer different kinds of personal data about you which we have grouped together as follows:</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">Identity Data includes first name, maiden name, last name, username or similar title and date of birth.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Contact Data includes billing address, email address and telephone numbers.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Verification Data includes a copy of your passport, driver&rsquo;s license, national identity cards, a utility bill (for proof of address), and/or a copy of the front and back of your credit card that you are using in connection with your Account (we request that you cover and do not send us the credit card code verification (CVV) number; if we receive your CVV number we will use commercially reasonable efforts to delete and not store it and may ask that you resubmit a copy of your credit card without the CVV number being visible).</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Financial Data includes bank account and payment card details.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Transaction Data includes details about payments to and from you and other details of products and services you have purchased from us including billing information, invoice related information and other data required to process your order. We may also update such data should you grant us permission to bill you or your credit card for recurring charges, such as monthly or other types of periodic payments.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Technical Data includes internet protocol (IP) addresses, type of browser, Internet Service Provider (ISP), date/time stamp, referring/exit pages, clicked pages and any other information your browser may send to us.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">&nbsp;Profile Data includes your username and password, purchases or orders made by you, your interests, preferences, feedback and survey responses.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">&nbsp;Usage Data includes information about how you use our website, products and services.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Marketing and Communications Data includes your preferences in receiving marketing from us and our third parties and your communication preferences.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">AGGREGATED DATA</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We also collect, use and share Aggregated Data such as statistical or demographic data for any purpose. Aggregated Data may be derived from your personal data but is not considered personal data in law as this data does not directly or indirectly reveal your identity. For example, we may aggregate your Usage Data to calculate the percentage of users accessing a specific website feature. However, if we combine or connect Aggregated Data with your personal data so that it can directly or indirectly identify you, we treat the combined data as personal data which will be used in accordance with this privacy notice.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">SPECIAL CATEGORIES OF PERSONAL DATA</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Note that, unless you provide this to us yourselves, we do not request, nor do we collect, any Special Categories of Personal Data about you (this includes details about your race or ethnicity, religious or philosophical beliefs, sex life, sexual orientation, political opinions, trade union membership, information about your health and genetic and biometric data). In the same manner, we do not collect any information about criminal convictions and offences.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">COOKIES</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">To enable the working of certain functions during your visit to our website, we make use of cookies on various pages. Cookies are small text files that are stored on your device. We use two types of cookies: (i) &#39;session cookies&#39; which are stored temporarily during a browsing session in order to allow normal use of the system and are deleted from your device when the browser is closed; (ii) &#39;persistent cookies&#39; which are read only by our website, saved on your computer for a fixed period and are not deleted when the browser is closed. Such cookies are used where we need to know who you are for repeat visits, for example to allow us to store your preferences for the next sign-in.</span>
</p>
<p class="c2">
<span class="c0">You can set your browser to refuse all or some browser cookies, or to alert you when websites set or access cookies. If you disable or refuse cookies, please note that some parts of this website may become inaccessible or not function properly.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">IF YOU FAIL TO PROVIDE PERSONAL DATA</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Where we need to collect personal data by law, or under the terms of a contract we have with you and you fail to provide that data when requested, we may not be able to perform the contract we have or are trying to enter into with you (for example, to provide you with products or services). In this case, we may have to cancel a product or service you have with us but we will notify you if this is the case at the time.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">3. HOW IS YOUR PERSONAL DATA COLLECTED?</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We use different methods to collect data from and about you including through:</span>
</p>
<p class="c2">
<span class="c0">Direct interactions. You may give us your Identity, Contact, Financial Data and Verification Data by filling in forms or by corresponding with us by phone, email or otherwise. This includes personal data you provide when you:</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Create an account on our website.</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Apply for our services.</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Receive payments from us (for example, in connection with lottery winnings)</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Purchase certain products or services from us.</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Verify your account</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Send us a &ldquo;contact us&rdquo; or &ldquo;chat with us&rdquo; request;</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Request marketing to be sent to you;</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Enter a competition, promotion or survey; or</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Provide us with feedback.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Automated technologies or interactions. As you interact with our website, we may automatically collect Technical Data about your equipment, browsing actions and patterns. We collect this personal data by using cookies, logs files and other similar technologies.</span>
</p>
<p class="c2">
<span class="c0">Third parties or publicly available sources. We may receive personal data about you from various third parties and public sources as set out below:</span>
</p>
<p class="c2">
<span class="c0">Facebook Connect. You may engage with our content, such as video, lottery games, applications, and other offerings, through third-party social networking sites, such as Facebook. When you engage with our content on or through third party social networking sites, plug-ins or applications, you may allow us to have access to certain information from your public social media profile to deliver the content or as part of the operation of the application.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">4. HOW WE USE YOUR PERSONAL DATA</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We will only use your personal data when the law allows us to. Most commonly, we will use your personal data in the following circumstances:</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Where we need to perform the contract we are about to enter into or have entered into with you.</span>
</p>
<p class="c2">
<span class="c0">Where it is necessary for our legitimate interests (or those of a third party) and your interests and fundamental rights do not override those interests.</span>
</p>
<p class="c2">
<span class="c0">&nbsp;Where we need to comply with a legal or regulatory obligation.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Generally, we do not rely on consent as a legal basis for processing your personal data other than in relation to sending direct marketing communications to you via email, text message or phone. You have the right to withdraw consent to marketing at any time by contacting us.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">PURPOSES FOR WHICH WE WILL USE YOUR PERSONAL DATA</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We have set out below, in a table format, a description of all the ways we plan to use your personal data, and which of the legal bases we rely on to do so. We have also identified what our legitimate interests are where appropriate.</span>
</p>
<p class="c2">
<span class="c0">Note that we may process your personal data for more than one lawful ground depending on the specific purpose for which we are using your data. Please Contact us if you need details about the specific legal ground we are relying on to process your personal data where more than one ground has been set out in the table below.</span>
</p>
<p class="c2">
<span class="c0">Purpose/Activity Type of data Lawful basis for processing including basis of legitimate interest</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">To set up your account and provide our services;</span>
</p>
<p class="c2">
<span class="c0">(a) Performance of a contract with you to identify and authenticate your access to the services </span>
</p>
<p class="c2">
<span class="c0">(b) Contact</span>
</p>
<p class="c2">
<span class="c0">(c) Verification - Necessary to comply with a legal obligation</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">To prevent money laundering and terrorist financing (a) Identity</span>
</p>
<p class="c2">
<span class="c0">(b) Contact</span>
</p>
<p class="c2">
<span class="c0">(c) Verification</span>
</p>
<p class="c2">
<span class="c0">(d) Financial</span>
</p>
<p class="c2">
<span class="c0">(e) Transaction (a) Necessary to comply with a legal obligation</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">To process and deliver your order including:</span>
</p>
<p class="c3">
<span class="c0">(a) Manage payments, fees and charges</span>
</p>
<p class="c3">
<span class="c0">(b) Collect and recover money owed to u (a) Identity</span>
</p>
<p class="c3">
<span class="c0">(b) Contact</span>
</p>
<p class="c3">
<span class="c0">(c) Financial</span>
</p>
<p class="c3">
<span class="c0">(d) Transaction (a) Performance of a contract with you</span>
</p>
<p class="c3">
<span class="c0">(b) Necessary for our legitimate interests (to recover debts due to us)</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">To communicate with you and to keep you informed of our latest updates:</span>
</p>
<p class="c3">
<span class="c0">(a) Notifying you about changes to our terms or privacy notice</span>
</p>
<p class="c3">
<span class="c0">(b) Asking you to leave a review or take a survey</span>
</p>
<p class="c3">
<span class="c0">(c) Contact you in response to a &ldquo;contact us&rdquo; or &ldquo;chat with us&rdquo; request (a) Identity</span>
</p>
<p class="c3">
<span class="c0">(b) Contact</span>
</p>
<p class="c3">
<span class="c0">(c) Profile</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">Marketing and Communications </span>
</p>
<p class="c3">
<span class="c0">(a) Performance of a contract with you</span>
</p>
<p class="c3">
<span class="c0">(b) Necessary to comply with a legal obligation</span>
</p>
<p class="c3">
<span class="c0">(c) Necessary for our legitimate interests (to keep our records updated and to study how customers use our products/services)</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">To support and troubleshoot our Services and to respond to your queries</span>
</p>
<p class="c3">
<span class="c0">&nbsp;(a) Identity</span>
</p>
<p class="c3">
<span class="c0">(b) Contact</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">Technical </span>
</p>
<p class="c3">
<span class="c0">(a) Necessary for our legitimate interests:</span>
</p>
<p class="c3">
<span class="c0">&bull; for running our business</span>
</p>
<p class="c3">
<span class="c0">&bull; provision of administration and IT services</span>
</p>
<p class="c3">
<span class="c0">&bull; network security</span>
</p>
<p class="c3">
<span class="c0">&bull; to prevent fraud</span>
</p>
<p class="c3">
<span class="c0">&bull; in the context of a business reorganisation or group restructuring exercise</span>
</p>
<p class="c3">
<span class="c0">(b) Necessary to comply with a legal obligation</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">To perform a research or to conduct analytics in order to improve and customize our Services to your needs and interests </span>
</p>
<p class="c3">
<span class="c0">(a) Identity</span>
</p>
<p class="c3">
<span class="c0">(b) Contact</span>
</p>
<p class="c3">
<span class="c0">(c) Profile</span>
</p>
<p class="c3">
<span class="c0">(d) Technical</span>
</p>
<p class="c3">
<span class="c0">(e) Usage</span>
</p>
<p class="c3">
<span class="c0">(f) Marketing and Communications Necessary for our legitimate interests:</span>
</p>
<p class="c3">
<span class="c0">&bull; to study how customers use our products/services</span>
</p>
<p class="c3">
<span class="c0">&bull; to develop products/services</span>
</p>
<p class="c3">
<span class="c0">&bull; to define types of customers for our products and services</span>
</p>
<p class="c3">
<span class="c0">&bull; to keep our website updated and relevant</span>
</p>
<p class="c3">
<span class="c0">&bull; to grow our business and to inform our marketing strategy</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">To market our websites and products </span>
</p>
<p class="c3">
<span class="c0">(a) Identity</span>
</p>
<p class="c3">
<span class="c0">(b) Contact</span>
</p>
<p class="c3">
<span class="c0">(c) Technical</span>
</p>
<p class="c3">
<span class="c0">(d) Usage</span>
</p>
<p class="c3">
<span class="c0">(e) Profile</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">Marketing and Communications </span>
</p>
<p class="c3">
<span class="c0">(a)Necessary for our legitimate interests:</span>
</p>
<p class="c3">
<span class="c0">&bull; to develop our products/services</span>
</p>
<p class="c3">
<span class="c0">&bull; to grow our business</span>
</p>
<p class="c3">
<span class="c0">(b) Consent</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">To investigate violations and enforce our policies, and as required by law, regulation or other governmental authority, or to comply with a subpoena or similar legal process or respond to a government request.</span>
</p>
<p class="c3">
<span class="c0">(a) Identity</span>
</p>
<p class="c3">
<span class="c0">(b) Contact</span>
</p>
<p class="c3">
<span class="c0">(c) Financial</span>
</p>
<p class="c3">
<span class="c0">(d) Transaction</span>
</p>
<p class="c3">
<span class="c0">(e) Technical</span>
</p>
<p class="c3">
<span class="c0">(f) Profile</span>
</p>
<p class="c3">
<span class="c0">(g) Usage</span>
</p>
<p class="c3">
<span class="c0">(h) Marketing and communications</span>
</p>
<p class="c3">
<span class="c0">(a) Necessary to comply with a legal obligation</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">MARKETING</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">We strive to provide you with choices regarding certain personal data uses, particularly around marketing and advertising. You can review and make certain decisions about the use of your personal data in the My Account section of the website.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">PROMOTIONAL OFFERS FROM US</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We may use your Identity, Contact, Technical, Usage and Profile Data to form a view on what we think you may want or need, or what may be of interest to you. This is how we decide which products, services and offers may be relevant for you (we call this marketing).</span>
</p>
<p class="c2">
<span class="c0">You will receive marketing communications from us if you have requested information from us or purchased products or services from us and, in each case, you have not opted out of receiving that marketing.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">OPTING OUT</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">You can ask us to stop sending you marketing messages at any time by logging into the website and checking or unchecking relevant boxes to adjust your marketing preferences or by following the opt-out links on any marketing message sent to you or by Contacting us at any time.</span>
</p>
<p class="c2">
<span class="c0">Where you opt out of receiving these marketing messages, this will not apply to personal data provided to us as a result of a product/service purchase, warranty registration, product service experience or other transactions.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">CHOICE</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">At all times, you may choose whether or not to provide or disclose Personal Information. The notices that we provide on the Site in circumstances where we collect Personal Information should help you to make this choice. If you choose not to provide the Personal Information we request, you may still visit parts of the Site, but you may be unable to access certain options, programs, offers, and services that involve our interaction with you.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">CHANGE OF PURPOSE</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We will only use your personal data for the purposes for which we collected it, unless we reasonably consider that we need to use it for another reason and that reason is compatible with the original purpose. If you wish to get an explanation as to how the processing for the new purpose is compatible with the original purpose, please Contact us.</span>
</p>
<p class="c2">
<span class="c0">If we need to use your personal data for an unrelated purpose, we will notify you and we will explain the legal basis which allows us to do so.</span>
</p>
<p class="c2">
<span class="c0">Please note that we may process your personal data without your knowledge or consent, in compliance with the above rules, where this is required or permitted by law.</span>
</p>
<p class="c2">
<span class="c0">5. DISCLOSURES OF YOUR PERSONAL DATA</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We do not rent, sell, or share your Personal Information with third parties except as described in this Privacy Notice.</span>
</p>
<p class="c2">
<span class="c0">We may have to share your personal data with the parties set out below for the purposes set out in the table in paragraph 4 above.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; Internal Third Parties as set out in the Glossary.</span>
</p>
<p class="c3">
<span class="c0">&nbsp; &nbsp; External Third Parties as set out in the Glossary.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">GOOD FAITH DISCLOSURE</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We may also disclose your Personal Information or any information you submitted via the Site if we have a good faith belief that disclosure of such information is helpful or reasonably necessary to (i) comply with any applicable law, regulation, legal process or governmental request; (ii) enforce our Terms including investigations of potential violations thereof; (iii) detect, prevent, or otherwise address fraud or security issues; or (iv) protect against harm to the rights, property or safety of the Company, its users, yourself or the public.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">MERGER, SALE OR BANKRUPTCY</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">In the event that the Company is acquired by or merged with a third party entity, we reserve the right to transfer or assign Personal Information that we have collected as part of such merger, acquisition, sale, or other change of control. In the unlikely event of our bankruptcy, insolvency, reorganization, receivership, or assignment for the benefit of creditors, or the application of laws or equitable principles affecting creditors&#39; rights generally, we may not be able to control how Personal Information is treated, transferred, or used. In any of the circumstances described in this paragraph, the information will be subject to the privacy notice of the purchaser or assignee.</span>
</p>
<p class="c2">
<span class="c0">We require all third parties to respect the security of your personal data and to treat it in accordance with the law. We do not allow our third-party service providers to use your personal data for their own purposes and only permit them to process your personal data for specified purposes and in accordance with our instructions.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">6. INTERNATIONAL TRANSFERS</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We share your personal data within the Company and with third party service providers. This will involve transferring your data outside the European Economic Area ( EEA).</span>
</p>
<p class="c2">
<span class="c0">Whenever we transfer your personal data out of the EEA, we ensure a similar degree of protection is afforded to it by ensuring at least one of the following safeguards is implemented:</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We will only transfer your personal data to countries that have been deemed to provide an adequate level of protection for personal data by the European Commission. For further details, see European Commission: Adequacy of the protection of personal data in non-EU countries.</span>
</p>
<p class="c2">
<span class="c0">Where we use certain service providers, we may use specific contracts approved by the European Commission which give personal data the same protection it has in Europe. For further details, see European Commission: Model contracts for the transfer of personal data to third countries.</span>
</p>
<p class="c2">
<span class="c0">Where we use providers based in the US, we may transfer data to them if they are part of the Privacy Shield which requires them to provide similar protection to personal data shared between the Europe and the US. For further details, see European Commission: EU-US Privacy Shield.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">Please Contact us if you want further information on the specific mechanism used by us when transferring your personal data out of the EEA.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">7. DATA SECURITY</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">The security of Personal Information is important to us. We follow generally accepted industry standards, including the use of appropriate administrative, physical and technical safeguards, to prevent your personal data from being accidentally lost, used or accessed in an unauthorised way, altered or disclosed. In addition, we limit access to your personal data to those employees, agents, contractors and other third parties who have a business need to know. They will only process your personal data on our instructions and they are subject to a duty of confidentiality.</span>
</p>
<p class="c2">
<span class="c0">To the extent that we collect sensitive Personal Information from you (for example, your credit card number or other financial Personal Information), we use SSL encryption to protect that information. However, no method of transmission over the Internet, or method of electronic storage, is 100% secure. Therefore, while we strive to use commercially acceptable means to protect your Personal Information, we cannot guarantee its absolute security. If you have any questions about security on the Site, you can contact us.</span>
</p>
<p class="c2">
<span class="c0">We have put in place procedures to deal with any suspected personal data breach and will notify you and any applicable regulator of a breach where we are legally required to do so.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">8. DATA RETENTION</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">HOW LONG WILL YOU USE MY PERSONAL DATA FOR?</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We will only retain your personal data for as long as necessary to fulfil the purposes we collected it for, including for the purposes of satisfying any legal, accounting, or reporting requirements.</span>
</p>
<p class="c2">
<span class="c0">To determine the appropriate retention period for personal data, we consider the amount, nature, and sensitivity of the personal data, the potential risk of harm from unauthorised use or disclosure of your personal data, the purposes for which we process your personal data and whether we can achieve those purposes through other means, and the applicable legal requirements.</span>
</p>
<p class="c2">
<span class="c0">Details of retention periods for different aspects of your personal data are available in our retention policy which you can request from us by Contacting us.</span>
</p>
<p class="c2">
<span class="c0">Please note that unless you instruct us otherwise we retain the information we collect for as long as needed to provide the Services and to comply with our legal obligations, resolve disputes and enforce our agreements.</span>
</p>
<p class="c2">
<span class="c0">In some circumstances you can ask us to delete your data: see Request erasure below for further information.</span>
</p>
<p class="c2">
<span class="c0">We may rectify, replenish or remove incomplete or inaccurate information, at any time and at our own discretion.</span>
</p>
<p class="c2">
<span class="c0">In some circumstances we may anonymise your personal data (so that it can no longer be associated with you) for research or statistical purposes in which case we may use this information indefinitely without further notice to you.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">9. YOUR LEGAL RIGHTS</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Under certain circumstances, you have rights under data protection laws in relation to your personal data. Please click on the links below to find out more about these rights:</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Request access to your personal data.</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Request correction of your personal data.</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Request erasure of your personal data.</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Object to processing of your personal data.</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Request restriction of processing your personal data.</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Request transfer of your personal data.</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Right to withdraw consent.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">If you wish to exercise any of the rights set out above, please Contact us.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">NO FEE USUALLY REQUIRED</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">You will not have to pay a fee to access your personal data (or to exercise any of the other rights). However, we may charge a reasonable fee if your request is clearly unfounded, repetitive or excessive. Alternatively, we may refuse to comply with your request in these circumstances.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">WHAT WE MAY NEED FROM YOU</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We may need to request specific information from you to help us confirm your identity and ensure your right to access your personal data (or to exercise any of your other rights). This is a security measure to ensure that personal data is not disclosed to any person who has no right to receive it. We may also contact you to ask you for further information in relation to your request to speed up our response.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">TIME LIMIT TO RESPOND</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We try to respond to all legitimate requests within one month. Occasionally it may take us longer than a month if your request is particularly complex or you have made a number of requests. In this case, we will notify you and keep you updated.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">COMMITMENT</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We are committed to protecting your privacy. Protecting your privacy online is an evolving area, and we are constantly evolving our Site to meet these demands. If you have any comments or questions regarding our Privacy Notice, please contact us.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">10. GLOSSARY</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">LAWFUL BASIS</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Legitimate Interest means the interest of our business in conducting and managing our business to enable us to give you the best service/product and the best and most secure experience. We make sure we consider and balance any potential impact on you (both positive and negative) and your rights before we process your personal data for our legitimate interests. We do not use your personal data for activities where our interests are overridden by the impact on you (unless we have your consent or are otherwise required or permitted to by law). You can obtain further information about how we assess our legitimate interests against any potential impact on you in respect of specific activities by Contacting us</span>
</p>
<p class="c2">
<span class="c0">Performance of Contract means processing your data where it is necessary for the performance of a contract to which you are a party or to take steps at your request before entering into such a contract.</span>
</p>
<p class="c2">
<span class="c0">Comply with a legal or regulatory obligation means processing your personal data where it is necessary for compliance with a legal or regulatory obligation that we are subject to.</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">THIRD PARTIES</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">INTERNAL THIRD PARTIES</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Other companies in the Group acting as processors and who are based in Romania and provide IT and system administration services and undertake leadership reporting.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c3">
<span class="c0">EXTERNAL THIRD PARTIES</span>
</p>
<p class="c3 c4">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">We may need to request specific information from you to help us confirm your identity and ensure your right to access your personal data (or to exercise any of your other rights). This is a security measure to ensure that personal data is not disclosed to any person who has no right to receive it. We may also contact you to ask you for further information in relation to your request to speed up our response.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">Service providers acting as processors based in Israel who provide IT and system administration services.</span>
</p>
<p class="c2">
<span class="c0">Service providers such as WalkMe and Hotjar, acting a processors based in Israel and Malta who provide marketing services;</span>
</p>
<p class="c2">
<span>Service providers such as Data Dog and Humio acting a processors based in Israel and Malta who provide logging tools;</span>
</p>
<p class="c2">
<span>&nbsp; &nbsp;</span>
<span class="c7 c9">&nbsp;Service providers such as Twillio, BriteVerify, SendGrid, Google Cloud Messaging (GCM), Apple Push Notification service (APNs), and Trustpilot acting as processors based in Ireland, UK and Denmark who provide communication services;</span>
</p>
<p class="c2">
<span class="c7 c9">&nbsp; &nbsp; Service providers acting as processors based in Germany and the UK who provide payment services.</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Professional advisers acting as processors including lawyers, bankers, auditors and insurers based in Malta who provide consultancy, banking, legal, insurance and accounting services.</span>
</p>
<p class="c2">
<span class="c0">&nbsp; &nbsp; Inland Revenue department, regulators and other authorities acting as processors or joint controllers based in Malta who require reporting of processing activities in certain circumstances.</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">YOUR LEGAL RIGHTS</span>
</p>
<p class="c1">
<span class="c0">
</span>
</p>
<p class="c2">
<span class="c0">You have the right to:</span>
</p>
<p class="c2">
<span class="c0">Request access to your personal data (commonly known as a &ldquo;data subject access request&rdquo;). This enables you to receive a copy of the personal data we hold about you and to check that we are lawfully processing it.</span>
</p>
<p class="c2">
<span class="c0">Request correction of the personal data that we hold about you. This enables you to have any incomplete or inaccurate data we hold about you corrected, though we may need to verify the accuracy of the new data you provide to us.</span>
</p>
<p class="c2">
<span class="c0">Request erasure of your personal data. This enables you to ask us to delete or remove personal data where there is no good reason for us continuing to process it. You also have the right to ask us to delete or remove your personal data where you have successfully exercised your right to object to processing (see below), where we may have processed your information unlawfully or where we are required to erase your personal data to comply with local law. Note, however, that we may not always be able to comply with your request of erasure for specific legal reasons which will be notified to you, if applicable, at the time of your request.</span>
</p>
<p class="c2">
<span class="c0">Object to processing of your personal data where we are relying on a legitimate interest (or those of a third party) and there is something about your particular situation which makes you want to object to processing on this ground as you feel it impacts on your fundamental rights and freedoms. You also have the right to object where we are processing your personal data for direct marketing purposes. In some cases, we may demonstrate that we have compelling legitimate grounds to process your information which override your rights and freedoms.</span>
</p>
<p class="c2">
<span class="c0">Request restriction of processing of your personal data. This enables you to ask us to suspend the processing of your personal data in the following scenarios: (a) if you want us to establish the data&rsquo;s accuracy; (b) where our use of the data is unlawful but you do not want us to erase it; (c) where you need us to hold the data even if we no longer require it as you need it to establish, exercise or defend legal claims; or (d) you have objected to our use of your data but we need to verify whether we have overriding legitimate grounds to use it.</span>
</p>
<p class="c2">
<span class="c0">Request the transfer of your personal data to you or to a third party. We will provide to you, or a third party you have chosen, your personal data in a structured, commonly used, machine-readable format. Note that this right only applies to automated information which you initially provided consent for us to use or where we used the information to perform a contract with you.</span>
</p>
<p class="c2">
<span class="c0">Withdraw consent at any time where we are relying on consent to process your personal data. However, this will not affect the lawfulness of any processing carried out before you withdraw your consent. If you withdraw your consent, we may not be able to provide certain products or services to you. We will advise you if this is the case at the time you withdraw your consent.</span>
</p>


                </div>
            </div>
        </div>
    </section>
</div>

<?php include('assets/comp/footer-guest.php'); ?>

</body>
</html>
