<?php
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();
//REDIRECT ADMIN
if( !$admin_user ) { header( 'Location: dashboard' ); exit; }

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<div id="page-content-wrapper">
		<div class="container-fluid">

			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?php echo $lang['RECURRING_COMMISSIONS']; ?></span>
						</div>
						<div class="panel-content">
							<div class="alert alert-info">
								<?php echo $lang['RECURRING_DESCRIPTION']; ?>
							</div>
							<form method="post" action="data-functions/update-rc">
								Enable <?php echo $lang['RECURRING_COMMISSIONS']; ?> <input type="checkbox" name="rc_on"
																							value="1"
																							data-on-color="success"
																							data-off-color="danger"
									<?php $rc_on = rc_on();
									if( $rc_on == '1' ) {
										echo 'checked';
									} ?>>
								<input type="submit" class="btn btn-success" value="Save Settings">
							</form>
							<hr>
							<strong>Recurring commissions are initialized the same way fixed commissions are with the
								addition of the two recurring variables below: </strong>
							<code>
								<pre>$sale_amount = '21.98';<br>$product = 'My Product Description';<br><strong>$recurring = 'monthly';</strong> //can be daily, weekly, biweekly, or monthly<br><strong>$recurring_fee = '10';</strong> //percentage of original sale<br>include('affiliate-pro/controller/record-sale.php);</pre>
							</code>
							<strong>Important: Recurring commissions requires you to setup the following cron job to
								work.</strong>
							<code>
								<pre>0  1  *  *  *  /usr/bin/php -q /home/username/public_html/affiliate-pro/data-functions/recurring.php</pre>
							</code>
						</div>
					</div>
				</div>
				<!-- End Panel -->
			</div>
		</div>
	</div>
	<!-- End Page Content -->

</div><!-- End Main Wrapper  -->

<?php include('assets/comp/footer.php'); ?>

<script>
	$(document).ready(function () {
		$('#users').DataTable();
	});

	$("[name='rc_on']").bootstrapSwitch();

	<?php
	if( isset($_SESSION['action_saved']) ) {
		echo 'swal("Awesome Work!", "Your changes have been applied!", "success")';
	}
	if( isset($_SESSION['action_deleted']) ) {
		echo 'swal("Deleted", "This has been deleted as requested!", "success")';
	}
	unset($_SESSION['action_saved']);
	unset($_SESSION['action_deleted']);
	?>
</script>

</body>
</html>
