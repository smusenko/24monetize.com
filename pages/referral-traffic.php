<?php
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();
include('assets/comp/header.php');
$cpc_on = cpc_on();
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<div id="page-content-wrapper">
		<div class="container-fluid">
			<?php include('assets/comp/referral-stat-boxes.php'); ?>
			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-6">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?php echo $lang['TOP_REFERRING_URLS']; ?></span>
							<div class="date-filter pull-right">
								<form method="post" action="data-functions/set-filter">
									<input type="hidden" name="redirect" value="/<?= $pg ?>">
									From <input type="date" name="start_date" value="<?php echo $start_date; ?>">
									to <input type="date" name="end_date" value="<?php echo $end_date; ?>">
									<input type="submit" class="btn btn-xs btn-primary" value="Filter">
								</form>
							</div>
						</div>
						<div class="panel-content" style='overflow-x:auto;clear:both'>
							<div id="status"></div>
							<table id="tru" class="row-border" cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?= $lang['AFFILIATE']; ?></th>
									<th><?= $lang['LANDING_PAGE']; ?></th>
								</tr>
								</thead>

								<tbody>
								<?php top_url_referral_table( $start_date, $end_date ); ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End Panel -->
				<div class="col-lg-6">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?php echo $lang['TOP_REFERRING_AFFILIATES']; ?></span>
							<div class="date-filter pull-right">
								<form method="post" action="data-functions/set-filter">
									<input type="hidden" name="redirect" value="/<?= $pg ?>">
									From <input type="date" name="start_date" value="<?php echo $start_date; ?>">
									to <input type="date" name="end_date" value="<?php echo $end_date; ?>">
									<input type="submit" class="btn btn-xs btn-primary" value="Filter">
								</form>
							</div>
						</div>
						<div class="panel-content">
							<table id="tra" class="row-border" cellspacing="0" width="100%">
								<thead>
								<tr>
									<th><?php echo $lang['AFFILIATE']; ?></th>
									<th><?php echo $lang['REFERRED_VISITS']; ?></th>
									<?= $cpc_on==1 ? '<td>'.$cpc_on.$lang['CPC_EARNINGS'].'</td>' : '' ?>
								</tr>
								</thead>

								<tbody>
								<?php top_referring_affiliates( $start_date, $end_date ); ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
				<!-- End Panel -->

				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?php echo $lang['ALL_REFERRAL_TRAFFIC']; ?></span>
							<div class="date-filter pull-right">
								<form method="post" action="data-functions/set-filter">
									<input type="hidden" name="redirect" value="/<?= $pg ?>">
									From <input type="date" name="start_date" value="<?php echo $start_date; ?>">
									to <input type="date" name="end_date" value="<?php echo $end_date; ?>">
									<input type="submit" class="btn btn-xs btn-primary" value="Filter">
								</form>
							</div>
						</div>
						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="art" class="row-border" cellspacing="0" width="100%">
									<thead>
									<tr>
										<th><?= $lang['AFFILIATE'] ?></th>
										<th><?= $lang['IP_ADDRESS'] ?></th>
										<th><?= $lang['BROWSER'] ?></th>
										<th><?= $lang['OS'] ?></th>
										<th><?= $lang['COUNTRY'] ?></th>
										<th><?= $lang['DEVICE'] ?></th>
										<th><?= $lang['LANDING_PAGE'] ?></th>
										<th><?= $lang['CPC_EARNINGS'] ?></th>
										<th><?= $lang['DATETIME'] ?></th>
										<th><?= $lang['ACTION'] ?></th>
									</tr>
									</thead>
								</table>
							</div>
						</div>
					</div>
					<!-- End Panel -->
				</div>
			</div>
		</div>
		<!-- End Page Content -->

	</div><!-- End Main Wrapper  -->

	<?php include('assets/comp/footer.php'); ?>

	<script>

		$(document).ready(function () {
			$('#tru').DataTable();
			$('#tra').DataTable();
		});

		$(document).ready(function () {
			$('#art').dataTable({
				<?= $cpc_on ? '':'"columnDefs": [{ "targets":[7], "visible":false }],' ?>
				"aaSorting": [[8,'desc']], // def: datetime desc
				"bProcessing": true,
				"bServerSide": true,
				"sAjaxSource": "/data-functions/ajax_referral-data.php",
				"aoColumns": [{"sName":"1"},{"sName":"2"},{"sName":"3"},{"sName":"4"},{"sName":"5"},{"sName":"6"},{"sName":"7"},{"sName":"cpc"},{"sName":"datetime"},{"sName":"action","bSortable":false}]
			});
		});
	</script>

</body>
</html>