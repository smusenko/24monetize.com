<?php
include('includes/startup.php');
include('includes/register.inc.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include('assets/comp/header-guest.php');

?>
<body>
<!-- Page Content -->
<div class="container">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-md-offset-3">

			<div style='padding:30px'><a href='/'><?= site_logo(220); ?></a></div>
			<form method="post" action="data-functions/reset-password" class="form-horizontal login-form">
				<fieldset>
					<input type="hidden" name="key"
						   value="<?php $key = filter_input( INPUT_GET, 'k', FILTER_SANITIZE_STRING );
						   echo $key; ?>">
					<?php
					if( @$_GET['error'] == '1' ) {
						echo '<span class="red">New password and Confirm password do not match</span>';
					}
					if( @$_GET['error'] == '2' ) {
						echo '<span class="red">Invalid PIN entered.  Please refer to the reset email for the correct PIN.</span>';
					}
					if( @$_GET['error'] == '3' ) {
						echo '<span class="red">You did not enter in the correct PIN or your secret key is missing.  Please use the link provided in the reset email.</span>';
					} ?>
					<h4>
						Reset your Password
					</h4>

					<div class="control-group">
						<label class="control-label" for="textinput">Temporary PIN</label>
						<div class="controls">
							<input id="textinput" name="pin" type="password" placeholder="See it in your email"
								   class="input-xlarge" required>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="textinput">New Password</label>
						<div class="controls">
							<input id="textinput" name="new_pass" type="password" placeholder="" class="input-xlarge"
								   required>
						</div>
					</div>
					<div class="control-group">
						<label class="control-label" for="textinput">Confirm New Password</label>
						<div class="controls">
							<input id="textinput" name="c_pass" type="password" placeholder="" class="input-xlarge"
								   required>
						</div>
					</div>

					<div class="control-group forgot-link">
						<a href="https://affiliates.24monetize.com" data-old="login">Don`t need to reset your password? Go login now</a>
					</div>

					<!-- Submit-->
					<div class="control-group">
						<div class="controls">
							<input type="submit" class="btn btn-primary btn-block login-btn" value="Reset Password">
						</div>
					</div>
				</fieldset>
			</form>
		</div>
	</div>
</div>
<!-- /.container -->

<!-- jQuery -->
<script src="assets/js/jquery.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="assets/js/bootstrap.min.js"></script>

</body>
</html>
