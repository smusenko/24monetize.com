<?php
// copied from sales-profits.php and updated
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<!-- YOUR CONTENT GOES HERE -->
	<div id="page-content-wrapper">
		<div class="container-fluid">

			<?php include('assets/comp/sales-stat-boxes-user.php'); ?>

			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary floatfix">
							<span class="title"><?php echo $lang['REVENUE_COMISSION']; ?></span>
							<div class="pull-right no-text-shadow">
								<div id="set-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive"
									data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="fa-calendar"></i>
									<span></span>
									<i class="fa-angle-down"></i>
									<form method="post" action="data-functions/set-filter" class='date-form hidden'>
										<input type="hidden" name="redirect" value="/<?= $pg ?>">
										<input type="text" name="start_date" value="<?php echo $start_date; ?>">
										<input type="text" name="end_date" value="<?php echo $end_date; ?>">
									</form>
								</div>
							</div>
						</div>
						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="sales" class="row-border" cellspacing="0" width="100%">
									<thead>
									<tr>
										<th><?= $lang['DETAILS']; ?></th>
										<th><?= $lang['REVENUE']; ?></th>
										<th><?= $lang['COMISSION']; ?></th>
										<th><?= $lang['NET_EARNINGS']; ?></th>
										<?php
										$rc_on = rc_on();
										if( $rc_on == '1' ) echo '<th>'.$lang['RECURRING'].'</th>';
										?>
										<th><?= $lang['DATETIME']; ?></th>
									</tr>
									</thead>

									<tbody>
									<?= my_sales_table( $start_date, $end_date, ($admin_user=='1' ? 0:$owner) ); ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- End Panel -->
				</div>
			</div>
		</div>
		<!-- End Page Content -->

	</div><!-- End Main Wrapper  -->

	<?php include('assets/comp/footer.php'); ?>

<script src='/assets/js/handleDaterangepicker.js'></script>
<script>
	var _startDate=new Date(<?= 1000*strtotime($start_date) ?>), _endDate=new Date(<?= 1000*strtotime($end_date) ?>)
	$(document).ready(function () {
		$('#sales').dataTable({ "aaSorting": [] /* Disable initial sort */ });
		handleDateRangePicker('#set-report-range',_startDate,_endDate);
	})

	<?php
	if( isset($_SESSION['action_deleted']) ) {
		echo 'swal("Deleted", "This has been deleted as requested!", "success")';
	}
	unset($_SESSION['action_deleted']);
	?>
</script>

</body>
</html>
