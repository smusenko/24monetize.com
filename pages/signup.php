<?php
// external Aff, 2020-01-13, Sergey
header("HTTP/1.1 301");
header( "location: https://affiliates.24monetize.com/signup" );
exit;


include( 'includes/startup.php' );
include( 'includes/register.inc.php' );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include( 'assets/comp/header-guest.php' );
?>

<body>
<div class='bodybg'>

	<?php include('assets/comp/top-nav-guest.php'); ?>

	<!-- Page Content -->
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<form method="post" class="form-horizontal login-form">
					<fieldset>
						<legend>Join</legend>
						<?= '<div style="color:red;">'.$error_msg.'</div>'; ?>
						<div class="control-group">
							<label class="control-label" for="textinput1">Full Name</label>
							<div class="controls">
								<input id="textinput1" name="fullname" type="text" placeholder="Full Name"
									   class="input-xlarge" value="<?= @$_POST['fullname']; ?>"
									   required="required">
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="textinput2">Username</label>
							<div class="controls">
								<input id="textinput2" name="username" type="text" placeholder="username"
									   class="input-xlarge" value="<?= @$_POST['username']; ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="textinput3">E-Mail Address</label>
							<div class="controls">
								<input id="textinput3" name="email" type="email" placeholder="email@provider.com"
									   class="input-xlarge" value="<?= @$_POST['email']; ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="textinput5">Your Website URL</label>
							<div class="controls">
								<input id="textinput5" name="websites" type="text" placeholder="http://www.website.com"
									   class="input-xlarge" value="<?= @$_POST['websites']; ?>" required>
							</div>
						</div>

						<!-- Password input-->
						<div class="control-group">
							<label class="control-label" for="passwordinput1">Password</label>
							<div class="controls">
								<input id="passwordinput1" name="p" type="password" placeholder="password"
									   class="input-xlarge" value="<?= @$_POST['qp']; ?>" required>
							</div>
						</div>

						<div class="control-group">
							<label class="control-label" for="passwordinput2">Confirm Password</label>
							<div class="controls">
								<input id="passwordinput2" name="confirmpwd" type="password" placeholder="Confirm"
									   class="input-xlarge" value="<?= @$_POST['qcp']; ?>" required>
							</div>
						</div>

						<div class="control-group terms">
							<div class="controls">
								<input id='chk1' type="checkbox" value="1" name="terms" <?= @$_POST['terms']=='1' ? 'checked':'' ?>>
								<label for='chk1' style='display:inline; text-transform:none'> I accept the terms of the affiliate policy</label>
							</div>
						</div>

						<!-- Submit-->
						<div class="control-group">
							<div class="controls">
								<input type="submit" class="btn btn-warning btn-block register-btn"
									   value="Create Account">
							</div>
						</div>
					</fieldset>
				</form>
			</div>

			<div class="col-lg-6">
				<form method="post" action="data-functions/process_login" class="form-horizontal login-form">
					<fieldset>
						<legend>Login</legend>
						<?php if( @$_GET['logoff'] == '1' ) {
							echo '<span class="success-text">You have logged off successfully</span>';
						}
						if( @$_GET['error'] == '1' ) {
							echo '<span class="red">Invalid Username or Password</span>';
						} ?>
						<!-- Username -->
						<div class="control-group">
							<label class="control-label" for="textinput4">Username or E-mail Address</label>
							<div class="controls">
								<input id="textinput4" name="email" type="text" placeholder="demo" class="input-xlarge">
							</div>
						</div>

						<!-- Password input-->
						<div class="control-group">
							<label class="control-label" for="passwordinput3">Password</label>
							<div class="controls">
								<input id="passwordinput3" name="p" type="password" placeholder="Password"
									   class="input-xlarge">
							</div>
						</div>

						<div class="control-group forgot-link">
							<a href="forgot">Forgot your username or password?</a>
						</div>

						<!-- Submit-->
						<div class="control-group">
							<div class="controls">
								<input type="submit" class="btn btn-primary btn-block login-btn" value="Login">
							</div>
						</div>
					</fieldset>
				</form>
<?php /*
<div class="affiliate-description">
	<h1><i class="fa fa-help-circled"></i>Why Join our Affiliate Program?</h1>
	<ul>
		<li><i class="fa fa-ok-circled2"></i>Earn 15% on every sale you send us, Forever!</li>
		<li><i class="fa fa-ok-circled2"></i>24/7 Tracking, get a simple & easy dashboard.</li>
		<li><i class="fa fa-ok-circled2"></i>Maximize Your Revenue. Our expertise is in conversion and payments in Africa!</li>
		<li><i class="fa fa-ok-circled2"></i>Honesty, Transparency Counts. We pay 15% lifetime revenue share. We don`t break our promises. We`re here to stay.</li>
	</ul>
	<br>
</div>
*/ ?>
			</div>

		</div>
	</div>
	<!-- /.container -->

</div>

<?php include('assets/comp/footer-guest.php'); ?>

</body>
</html>
