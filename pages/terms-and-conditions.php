<?php
include( 'includes/startup.php' );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include( 'assets/comp/header-guest.php' );
?>

<style type="text/css">ol{margin:0;padding:0}table td,table th{padding:0}.c1{background-color:#ffffff;color:#222222;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:"Arial";font-style:normal}.c0{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:justify;height:11pt}.c3{color:#000000;font-weight:400;text-decoration:none;vertical-align:baseline;font-size:11pt;font-family:"Arial";font-style:normal}.c2{padding-top:0pt;padding-bottom:0pt;line-height:1.15;orphans:2;widows:2;text-align:justify}.c6{text-decoration-skip-ink:none;-webkit-text-decoration-skip:none;color:#1155cc;text-decoration:underline}.c8{max-width:468pt;padding:72pt 72pt 72pt 72pt}.c4{background-color:#ffffff;color:#222222}.c5{color:inherit;text-decoration:inherit}.c7{background-color:#ffffff}.title{padding-top:0pt;color:#000000;font-size:26pt;padding-bottom:3pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}.subtitle{padding-top:0pt;color:#666666;font-size:15pt;padding-bottom:16pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}li{color:#000000;font-size:11pt;font-family:"Arial"}p{margin:0;color:#000000;font-size:11pt;font-family:"Arial"}h1{padding-top:20pt;color:#000000;font-size:20pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h2{padding-top:18pt;color:#000000;font-size:16pt;padding-bottom:6pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h3{padding-top:16pt;color:#434343;font-size:14pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h4{padding-top:14pt;color:#666666;font-size:12pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h5{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;orphans:2;widows:2;text-align:left}h6{padding-top:12pt;color:#666666;font-size:11pt;padding-bottom:4pt;font-family:"Arial";line-height:1.15;page-break-after:avoid;font-style:italic;orphans:2;widows:2;text-align:left}</style>
<body>
<div class='bodybg'>

	<?php include('assets/comp/top-nav-guest.php'); ?>

	<!-- Page Content -->
    <section class="section-white" style='margin-top:-30px'>
        <!--begin container-->
        <div class="container">

            <!--begin row-->
            <div class="row nopadding">
                <div class="center">
                    <span class="big-span">Terms And Conditions</span>
                </div>
                <div class="col-lg-12">


<p class="c0">
<span class="c1">
</span>
</p>
<p class="c2">
<span class="c4">This Affiliate Agreement together with the Affiliate Sign Up Form and any other guidelines or additional terms provided to you via email or our Website (together the &quot;Agreement&quot;) contain the complete terms and conditions that apply to your participation in </span>
<span class="c3">www.24monetize.com</span>
</p>
<p class="c2">
<span class="c1">&nbsp;(&quot;Affiliate Program&quot;). Where used in this Agreement, references to: (a) &quot;you&quot; &quot;your&quot; and/or &quot;Affiliate&quot; mean the individual or entity which applied as the &quot;BENEFICIARY&quot; for payment purposes on our sign up form as submitted at our Website (&quot;Affiliate Sign Up Form&quot;), and (b) &quot;we,&#39;&#39; &quot;our&quot;, &quot;us&quot; means 24Lottos enterprises Ltd.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&nbsp;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">1. GENERAL</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c4">1.1. &nbsp;This Agreement shall govern our relationship with you in relation to the Affiliate Program for the </span>
<span class="c6">
<a class="c5" href="https://www.google.com/url?q=http://www.24monetize.com&amp;sa=D&amp;ust=1579870099113000">www.24monetize.com</a>
</span>
<span>&nbsp;</span>
<span class="c1">website/s</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">1.2. When you indicate your acceptance of these terms and conditions on the Affiliate Sign up Form, you agree to all the terms and conditions set out in this Agreement (as amended or modified from time to time in accordance with Section 1.3 below).</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">1.3. We may modify any of the terms of this Agreement at any time, in our sole discretion, by either (i) emailing you a change notice or (ii) by posting the new version of the Agreement on our Website. Any such modification will only take effect 30 days after the date of posting or sending of any such notice (whichever is the earlier). If any modification is unacceptable to you, your only recourse is to terminate this Agreement. Your continued participation in the Affiliate Program following such 30 day period will be deemed binding acceptance of the modification.</span>
</p>
<p class="c0">
<span class="c1">
</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">2. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;DEFINITIONS AND INTERPRETATION</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">In this Agreement, references to the following words shall have the meanings set out below:</span>
</p>
<p class="c2">
<span class="c1">&quot;Account&quot; means a uniquely assigned account that is created for a Player when he/she successfully registers for the Services via a Tracker and makes an initial deposit.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&quot;Affiliate Fee&quot; is the amount due and payable to you, as calculated based solely on our system&#39;s data and in accordance with the terms of this Agreement and the fees and payments terms set forth in the Website (as may be changed from time to time by us in our sole discretion).</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&quot;Affiliate Section&quot; means the password-protected area of the Website that is accessible to you and which provides additional functionality, including facilities to check relevant statistics, update your profile, and create additional Trackers, select Banners and/or Text Links.</span>
</p>
<p class="c2">
<span class="c1">&nbsp;&quot;Fraud Traffic&quot; means deposits, revenues or traffic generated on the Services through illegal means or any other action committed in bad faith to defraud us (as determined by us in our sole discretion), regardless of whether or not it actually causes us harm, including deposits generated on stolen credit cards, collusion, manipulation of the service or system, bonuses or other promotional abuse, and unauthorized use of any third party accounts, copyrights, trademarks and other third party intellectual property rights (which for the avoidance of doubt includes our intellectual property rights) and any activity which constitutes Fraud Traffic under Section 3.7 below.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&quot;Marketing Materials&quot; means banners and text links (which includes Trackers which are made available by us on the Affiliate Section, that you may use to connect Players to our Services from your website) and any other marketing materials (which may include Our Marks) that have been provided or otherwise made available to you by us and/or pre-approved by us.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c4">&quot;Our Marks&quot; means the words &quot;24Monetize&quot;, and/or any logo, mark, domain name or trade name which contains, is confusingly similar to or is comprised of the </span>
<span class="c3">24monetize</span>
</p>
<p class="c2">
<span class="c1">&nbsp;name and mark or any other name or mark owned from time to time by us.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&nbsp;&quot;Player(s)&quot; means any person who has opened an Account through your Tracker who has not held an Account with us before and made a successful deposit.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&nbsp;&quot;Services&quot; means any product or service provided to Players on &nbsp;the 24Lottos Website.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&nbsp;&quot;Spam&quot; means any email or other electronic communication which you send which markets, promotes or which otherwise refers to us, the Website or our services from time to time, or which contains any Marketing Materials, Our Marks or Trackers and which breaches our Electronic Marketing Rules set forth in Section 5 below.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&nbsp; &quot;Tracker(s)&quot; means the unique Tracking URL Codes that we provide exclusively to you, through which we track Players&#39; and potential Players&#39; activities and calculate Affiliate Fees.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&nbsp; &quot;Tracking URL&quot; means a unique hyperlink or other linking tool for referencing our Website or Services through which you refer potential Players. When the relevant Player opens their Account, our system automatically logs the Tracking URL and records you as the Affiliate.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c4">&nbsp; &nbsp; &quot;Affiliate Program Website(s)&quot; means, the website located at the URL </span>
<span class="c6 c7">
<a class="c5" href="https://www.google.com/url?q=http://www.24monetize.com/&amp;sa=D&amp;ust=1579870099119000">www.24Monetize.com</a>
</span>
<span class="c1">&nbsp;and at any URL with which we replace such URL from time to time &nbsp;and each of its related pages.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">&nbsp;&quot;Restricted Territories&quot; means, territories that the site does not accept players from.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;TERMS &amp; CONDITIONS</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.1. You shall provide true and complete information to us when completing the Affiliate Sign Up Form &nbsp;and promptly update such information if all or any part of it changes. You shall also provide us with such other information as we may reasonably request from time to time.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2. You shall market to and refer potential Players to the 24Lottos Website. You will be solely liable for the content and manner of such marketing activities. All such marketing activities must be professional, proper and lawful under applicable rules, regulations or laws (including any laws relation to the content and nature of any advertising or marketing) and otherwise comply with the terms of this Agreement. You shall not authorize, assist or encourage any third party to:</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.1. Place Marketing Materials on any online site or other medium where the content and/or material on such website or medium is libelous, discriminatory, obscene, unlawful, sexually explicit, pornographic or violent or which is, in our sole discretion otherwise unsuitable;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.2. Develop and/or implement marketing and/or public relations strategies which have as their direct or indirect objective the targeting of marketing of us and/or the 24Lottos Website to any persons who are less than 18 years of age (or such higher age as may apply in the jurisdiction that you are targeting), regardless of the age of majority in the location you are marketing;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.3. Pace Marketing Materials on any online site or other medium, where the content and/or material on such online site or medium: (a) infringes any third party&#39;s intellectual property rights; (b) copies or resembles the 24Lottos Website in whole or in part; (c) disparages us or otherwise damages our goodwill or reputation in any way; or (d) frames any page of the 24Lottos Website in whole or in part;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.4. Read, intercept, modify, record, redirect, interpret, or fill in the contents of any electronic form or other materials submitted to us by any person;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.5. In any way alter, redirect or in any way interfere with the operation or accessibility of the 24Lottos Website or any page thereof;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.6. Register as a Player on behalf of any third party, or authorize or assist (save by promoting the 24Lottos Website and Services in accordance with this Agreement) any other person to register as a Player;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.7. Take any action that could reasonably cause any end-user confusion as to our relationship with you or any third party, or as to the ownership or operation of the site or service on which any functions or transactions are occurring;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.8. Post, serve or publish any advertisements, communications or promotional content promoting the 24Lottos Website, our Services or Our Marks or around or in conjunction with the display of the 24Lottos Website and/or any part or page thereof ;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.9. Cause the 24Lottos Website (or any parts or pages thereof) to open in a visitor&#39;s browser or anywhere else used for accessing the Services other than as a result of the visitor clicking on banners or text links contained in or as part of any Marketing Materials;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.10. Attempt to intercept or redirect (including via user-installed software) traffic from or on any online site or other place that participates in our Affiliate Program;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.11. Use any means to promote sites which resemble in any way the look and/or feel of the 24Lottos Website whether in whole or in part, nor utilize any such means or site to create the impression that such sites are the 24Lottos Website (or any part of the 24Lottos Website);</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.12. Violate the terms of use and any applicable policies of any search engines; or</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.2.13. Attempt to communicate to Players whether directly or indirectly to solicit them to move to any online site not owned by us or for other purposes without our prior approval.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c4">If we determine, in our sole discretion, that you have engaged in any of the foregoing activities, we may (without limiting any other rights or remedies available to us) withhold any Affiliate Fees and/or terminate this Agreement immediately on notice.</span>
</p>
<p class="c2">
<span class="c4">&nbsp;</span>
</p>
<p class="c2">
<span class="c1">3.3. &nbsp;Approved Marketing Materials. In providing the marketing activities referred to in Section 3.2, you shall only use the Marketing Materials. You shall not modify the Marketing Materials or Our Marks in any way without our prior written consent. You shall only use the Marketing Materials in accordance with the terms of this Agreement, any guidelines we provide to you on our Website or otherwise from time to time and any applicable laws. CDs and other customized promotional materials provided to you will be at your cost and deducted from Affiliate Fees. During the term of this Agreement, we grant you a terminable, non-exclusive, non-transferable right to use the Marketing Materials for the sole purpose of fulfilling your obligations under this Agreement.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.4.Competitive Marketing. You shall not be entitled to market to potential Players (i) on any Internet site on which we promote the 24Lottos Website; (ii) on any Internet search engine on which we promote the 24Lottos Website; and (iii) in any other manner which results in your competing with us in relation to the promotion of the 24Lottos Website.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.5. Non Assignment. Trackers are for your sole use and are not to be assigned to others without our written consent.</span>
</p>
<p class="c0">
<span class="c1">
</span>
</p>
<p class="c2">
<span class="c1">3.6. Commercial Use Only. This marketing opportunity is for commercial use only. You shall not register as a Player or make deposits to any Account (directly or indirectly) through your Tracker(s) for your own personal use and/or the use of your relatives, friends, employees, servants, agents or advisors, or otherwise attempt to artificially increase the Affiliate Fees payable to you or to defraud us. Violation of this provision shall be deemed to be Fraud Traffic.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.7. Player Information. We reserve the right to refuse service to any potential Player and to close the Account of any Player, at any time, in our sole discretion. All data relating to the Players shall, as between you and us, remain our exclusive property and you acquire no right to such information except pursuant to our express written instructions.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">3.8. Trademarks and Domain Names. You acknowledge that 24Monetize affiliates, own all intellectual property rights comprised in any and all of the Marketing Materials, &nbsp;the Website and Our Marks. Any use of any trade mark, domain name or trade name which contains, is confusingly similar to or is comprised of Our Marks (other than in accordance with the terms of this Agreement) without our prior written permission shall be unauthorized and further may constitute Fraud Traffic. By way of example, but without limitation, you may not register or use any of Our Marks in any part of any domain name. You agree that all use by you of Our Marks inures to our sole benefit and that you will not obtain any rights in Our Marks as a result of such use. You shall not register or attempt to register any trademarks or names that contain, are confusingly similar to or are comprised of Our Marks, and you hereby agree to transfer any such registration obtained by you to us upon demand. You further agree not to attack ownership of and title to Our Marks in any way.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;REPORTS &amp; PAYMENTS</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4.1. Reports. We will track and report Player activity for purposes of calculating your Affiliate Fees. The form, content and frequency of the reports may vary from time to time in our sole discretion. Generally, you will receive a monthly report with your payment indicating the total amount due to you after any deductions or set offs that we are entitled to make under this Agreement. In addition, daily reports will be available online for you to view new Players per Tracker.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4.2. Affiliate Fees. Unless otherwise agreed and subject to the terms of this Agreement and your full compliance with your obligations hereunder, Affiliate Fees accrued in any calendar month will be paid to you by the twentieth day of the following calendar month, after any deductions or set offs that we are entitled to make under this Agreement.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4.3. Minimum Payment and Time of Payment. Notwithstanding the foregoing , if for any calendar month the total amount due for all Trackers is less than or equal to US $1000, due to the costs and resources involved in administering the program and processing payments hereunder, the balance will be carried over and added to the next month&#39;s Affiliate Fees until the total amount is more than US $1000. Further, if the amount due is negative in any particular month, then that negative amount will carry over and be deducted against the following month.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4.4. Holdover for Fraud Traffic. In the event that, in our sole discretion, we suspect any Fraud Traffic, then we may delay payment of the Affiliate Fees to you for up to one hundred and eighty (180) days while we investigate and verify the relevant transactions. We are not obligated to pay Affiliate Fees in respect of Players who, in our sole discretion, are not verifiably who they claim to be or are otherwise involved with Fraud Traffic. In the event that we determine any activity to constitute Fraud Traffic, or to otherwise be in contravention of this Agreement, then in our sole discretion we may: (i) pay the Affiliate Fees in full, (ii) recalculate them in light of such suspected Fraud Traffic and/or (iii) forfeit your future Affiliate Fees in respect of Fraud Traffic (as appropriate).</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4.5. Method of Payment. All payments will be due and payable in United States Dollars or such other currency as we will determine. Payment will be made by wire transfer or any other method as we in our sole discretion decide; however we will use reasonable endeavors to accommodate your preferred payment method. Charges for will be covered by you and deducted from your Affiliate Fees. For the avoidance of doubt, we have no liability to pay any currency conversion charges or any charges associated with the transfer of money to your bank account.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4.6. Player Tracking and Active Players. You understand and agree that potential Players must link and make deposits through using your Tracker in order for you to receive Affiliate Fees. In no event, are we liable for your failure to use Trackers. Notwithstanding any other provision herein, we may at any time and in our sole discretion alter our tracking system and reporting format.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4.7. Disputes. If you disagree with the monthly reports or amount payable, do NOT accept payment for such amount and immediately send us written notice of your dispute. Dispute notices must be received within thirty (30) days of our making available your monthly report or your right to dispute such report or payment will be deemed waived and you shall have no claims in such regard. Further, payment of cheque, acceptance of payment transfer or acceptance of other payment from us by you will be deemed full and final settlement of Affiliate Fees due for the month indicated. Notwithstanding the foregoing, if any overpayment is made in the calculation of your Affiliate Fees we reserve the right to correct such calculation at any time and to reclaim from you any overpayment made by us to you.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">4.8. Money Laundering. You shall comply with all applicable laws and any policy notified by us through our Website or otherwise in relation to money laundering and/or the proceeds of crime.</span>
</p>
<p class="c0">
<span class="c1">
</span>
</p>
<p class="c2">
<span class="c1">4.9. The commission structure; This shall be into 3 parts. You are moved to the next level automatically once your commision amount exceeds your present level amount. </span>
</p>
<p class="c2">
<span class="c1">4.9.1. Up to $1000 - Your earnings shall be 25% net revenue. $1000 shall be the total net revenue from the sales of the products you have promoted.</span>
</p>
<p class="c0">
<span class="c1">
</span>
</p>
<p class="c2">
<span class="c1">4.9.2. From $1001 to $2000 - Your earnings shall be 30% net revenue. $1001 to $2000 shall be the total net revenue from the sales of the products you have promoted.</span>
</p>
<p class="c0">
<span class="c1">
</span>
</p>
<p class="c2">
<span class="c1">4.9.3. Over $2000- Your earnings shall be 35% net revenue. Over $2000 shall be the total net revenue from the sales of the products you have promoted.</span>
</p>
<p class="c0">
<span class="c1">
</span>
</p>
<p class="c0">
<span class="c1">
</span>
</p>
<p class="c2">
<span class="c1">5. &nbsp;ELECTRONIC MARKETING RULES</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">If you plan to promote 24Lottos Website through email marketing, then your email practices must comply with the following:</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.1. You have clear and specific consent from the proposed recipient(s) before you send any such communications. The consent must have been given to you by way of an opt-in consent mechanism. Any such tick box must not be pre-populated;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.2. The communication makes it clear that it is marketing 24Lottos Website and/or Services;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.3. If such communication includes any promotional offers (for example, the payment of free tickets to prospective Players) or any promotional competitions or games, that the promotion, competition or game is clearly identified as such and that any conditions which the prospective Player must meet in order to qualify for the promotion, competition or game are set out clearly and unambiguously in the communication;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.4.You do not send any such communications to persons under the age of 18 or (if higher), the age of majority in the country of the proposed recipient(s) of any such communication;</span>
</p>
<p class="c2">
<span>&nbsp;</span>
</p>
<p class="c2">
<span class="c1">5.5. You do not use any viral marketing techniques as part of your electronic marketing activities;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.6. Any such communications only promotes us, our Services and/or by the 24Lottos Website (and not any third parties, third party services and/or third party sites) and shall not include any content other than our Marketing Materials;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.7. You include a true name in the communication (e.g. the &quot;From&quot; line of any email) and not a sales pitch or marketing message. Any such communication must clearly identify you as the sender of the communication and you shall not falsify or otherwise attempt to hide your identity;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.8. You do not mislead the recipient(s) with regard to the content and purpose of the communication;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.9. You provide an adequate, functioning and conspicuous &quot;opt-out&quot; or &quot;unsubscribe&quot; option in every communication;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.10. The communication must include a valid address to which the recipient can respond to opt out/unsubscribe of future marketing communications. The reply address must be active for at least thirty days after sending the communication. You also include a physical business address in any such communication;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.11. You honor expeditiously any opt out/unsubscribe request made by any communication recipient. You must not send any further marketing communications to any person who indicates (by whatever means) that they do not wish to receive any further marketing communications;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.12. You include a link to your privacy policy in any such communication;</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.13. You do not send any such communications to any person who has registered on any applicable register of persons who do not wish to receive any marketing communications</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">5.14. Further, you are responsible for ensuring that your communications practices comply with all applicable laws and codes of practice, including: (i) European Directives 95/46 on the Protection of Individuals with regard to the Processing of Personal Data and 2002/58 on Privacy and Electronic Communications and any applicable local enactments thereof in relation to electronic marketing in the European Union; and (ii) the United States CAN-SPAM Act of 2003 (Public Law 108-187) in relation to any electronic marketing in the United States.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">6. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;TERM AND TERMINATION</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">6.1. Term and Termination. This Agreement will take effect when you indicate your acceptance of these terms and conditions on the Affiliate Sign Up Form &nbsp;and continue until terminated in accordance with the terms of this Agreement.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">6.2. Termination By You. You may terminate this Agreement, with or without cause, immediately upon written notice to us. In addition, you may cease marketing the theLotterWebsite anytime you want.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">6.3. Termination By Us. We may terminate this Agreement, with or without cause, upon thirty (30) days written notice to you. Further, we may terminate this Agreement immediately, without notice, if you materially breach this Agreement and do not cure within fifteen (15) days of notice to cure.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">6.4. Automatic Termination without notice. This Agreement shall be deemed automatically terminated without notice where: (a) the total cumulative balance of Affiliate Fees due to you is less than $1000 for six (6) consecutive calendar months; or (b) you do not have any persons qualifying as Players in any one hundred and eighty (180) day period.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">6.5. Effect of Termination. The following will apply upon termination of this Agreement: (a) you shall stop promoting the 24Lottos Website and all rights and licenses given to you under this Agreement will terminate immediately; (b) you shall return all confidential information and cease use of any of Our Marks and the Marketing Materials; (c) we may leave open, redirect or deactivate any Trackers in our sole discretion without any obligation to pay you for new Players who subsequently become Players; and (d) provided that we have paid or do pay to you such sums as are due at the date of termination which shall be subject to any rights we have to make deductions hereunder, we will have no further liability to pay you any further sums.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">7. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;LIABILITIES</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">7.1. No Warranties. We make no warranties or representations (whether express or implied by law, statute or otherwise) with respect to the affiliate program, our website, the 24lottos website, or any content, products or services available therein or related thereto or that 24Monetize or the 24lottos website, system, network, software or hardware (or that provided to us by third parties) will be error-free or uninterrupted or with respect to the quality, merchantability, fitness for a particular purpose or suitability of all or any of the foregoing. except as expressly stated otherwise in this agreement, all warranties, representations and implied terms and conditions are hereby excluded to the fullest extent permitted by law. furthermore, neither we (nor our providers or underlying vendors) are required to maintain redundant system(s), network, software or hardware.</span>
</p>
<p class="c0">
<span class="c1">
</span>
</p>
<p class="c2">
<span class="c1">7.2.Billing and Collection Limitations. We may, in our sole discretion, use any available means to block or restrict certain Players, deposits or play patterns or reject the applications of potential Players and/or Affiliates so as to reduce the number of fraudulent, unprofitable transactions or for any reason. We do not guarantee, represent or warrant the consistent application and/or success of any fraud prevention efforts.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">7.3.Liability Limitations. Our obligations under this Agreement do not constitute personal obligations of the owners, directors, officers, agents, employees, vendors or suppliers of the Website or the Services or the 24Lottos Website. Other than as expressly provided in this Agreement, in no event will we be liable for any direct, indirect, special, incidental, consequential or punitive loss, injury or damage of any kind (regardless of whether we have been advised of the possibility of such loss) including any loss of business, revenue, profits or data. Our liability arising under this Agreement, whether in contract, tort (including negligence) or for breach of statutory duty or in any other way shall only be for direct damages and shall not exceed the revenues generated and payable hereunder over the previous twelve months at the time that the event giving rise to the liability arises. However, nothing in this Agreement will operate to exclude or limit either party&#39;s liability for death or personal injury arising as a result of that party&#39;s negligence or for fraud.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">7.4. Indemnification. You shall defend, indemnify and hold us and our officers, directors, employees and representatives harmless on demand from and against any and all claims, demands, liabilities, losses, damages, costs and expenses (including reasonable legal fees) resulting or arising from your breach of this Agreement.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">7.5. Set off. Without prejudice to any other rights or remedies available to us under this Agreement or otherwise, we shall be entitled to set off any payments otherwise payable by us to you hereunder, against any liability of you to us, including any claims we have against you resulting from or arising from your breach of this Agreement.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">8. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;INDEPENDENT INVESTIGATION</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">8.1. Independent Investigation. You warrant that you have independently evaluated the desirability of marketing the 24Monetize website or service</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;MISCELLANEOUS</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.1.Notices. All notices pertaining to this Agreement will be given by email as follows: to you at the email address provided by you on the Affiliate Sign Up Form (or as subsequently updated by you to us in the event of change), and to us at MGMT@24Monetize.com. Any notice sent by email shall be deemed received on the earlier of an acknowledgement being sent or 24 hours from the time of transmission.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.2. Relationship of Parties. There is no relationship of exclusivity, partnership, joint venture, employment, agency or franchise between you or us under this Agreement. Neither party has the authority to bind the other (including the making of any representation or warranty, the assumption of any obligation or liability and/or the exercise of any right or power), except as expressly provided in this Agreement.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.3.Non-Exclusive. You understand that we may at any time (directly or indirectly), enter into marketing terms with other Affiliates on the same or different terms as those provided to you in this Agreement and that such Affiliates may be similar, and even competitive, to you. You understand that we may re-direct traffic and users from the 24Monetize Website to any other online site that we deem appropriate in our sole discretion, without any additional compensation to you.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.4.Confidentiality and Non Disclosure. As an Affiliate, you may receive confidential information from us, including confidential information as to our marketing plans, marketing concepts, structure and payments. This information is confidential to us and constitutes our proprietary trade secrets. You shall not disclose this information to third parties or use such information other than for the purposes of this Agreement without our prior written consent, save as expressly required by law (provided that any such disclosure is only to the extent so required).</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.5.Press. You may not issue any press release or other communication to the public with respect to this Agreement, Our Marks or your participation in this Affiliate Program without our prior written consent, except as required by law or by any legal or regulatory authority.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.6. Assignment. Except where you have received our prior written consent, you may not assign at law or in equity (including by way of a charge or declaration of trust), sub-license or deal in any other manner with this Agreement or any rights under this Agreement, or sub-contract any or all of your obligations under this Agreement, or purport to do any of the same. Any purported assignment in breach of this clause shall confer no rights on the purported assignee.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.7. Governing Law. This Agreement shall be governed by and construed in accordance with the laws of the United Kingdom without giving effect to conflicts of law principles. You irrevocably agree to submit, for the benefit of us, to the exclusive jurisdiction of the courts of London, United Kingdom, for the settlement of any claim, dispute or matter arising out of or concerning this Agreement or its enforceability and you waive any objection to proceedings in such courts on the grounds of venue or on the grounds that proceedings have been brought in an inconvenient forum.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.8. Severability. Whenever possible, each provision of this Agreement will be interpreted in such a manner as to be effective and valid under applicable law but, if any provision of this Agreement is held to be invalid, illegal or unenforceable in any respect, such provision will be ineffective only to the extent of such invalidity, or unenforceability, without invalidating the remainder of this Agreement or any other provision hereof.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.9. Entire Agreement. This Agreement embodies the complete agreement and understanding of the parties hereto with respect to the subject matter hereof and supersedes any prior agreement or understanding between the parties in relation to such subject matter. Each of the parties acknowledges and agrees that in entering into this Agreement, it has not relied on any statement, representation, guarantee warranty, understanding, undertaking, promise or assurance (whether negligently or innocently made) of any person (whether a party to this Agreement or not) other than as expressly set out in the Agreement. Each party irrevocably and unconditionally waives all claims, rights and remedies which but for this clause it might otherwise have had in relation to any of the foregoing. Nothing in this Section shall limit or exclude any liability for fraud.</span>
</p>
<p class="c0">
<span class="c3">
</span>
</p>
<p class="c2">
<span class="c1">9.10.Third Party Rights. Except insofar as this Agreement expressly provides that a third party may in their own right enforce a term of this Agreement, a person who is not a party to this Agreement has no right under local law or statute to rely upon or enforce any term of this Agreement but this does not affect any right or remedy of a third party which exists or is available apart from under that local law or statute.</span>
</p>


                </div>
            </div>
        </div>
    </section>
</div>

<?php include('assets/comp/footer-guest.php'); ?>

</body>
</html>
