<?php
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include('assets/comp/header.php');
$cpc_on = cpc_on();
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<!-- YOUR CONTENT GOES HERE -->
	<div id="page-content-wrapper">
		<div class="container-fluid">
			<?php include('assets/comp/referral-stat-boxes-user.php'); ?>
			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary floatfix">
							<span class="title"><?php echo $lang['ALL_REFERRAL_TRAFFIC']; ?></span>
							<div class="pull-right no-text-shadow">
								<div id="set-report-range" class="dashboard-date-range tooltips no-tooltip-on-touch-device responsive"
									data-tablet="" data-desktop="tooltips" data-placement="top" data-original-title="Change dashboard date range">
									<i class="fa-calendar"></i>
									<span></span>
									<i class="fa-angle-down"></i>
									<form method="post" action="data-functions/set-filter" class='date-form hidden'>
										<input type="hidden" name="redirect" value="/<?= $pg ?>">
										<input type="text" name="start_date" value="<?php echo $start_date; ?>">
										<input type="text" name="end_date" value="<?php echo $end_date; ?>">
									</form>
								</div>
							</div>
						</div>
						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="users" class="row-border" cellspacing="0" width="100%">
									<thead>
									<tr>
										<th><?= $lang['COUNTRY'] ?></th>
										<th><?= $lang['BROWSER'] ?></th>
										<th><?= $lang['LANDING_PAGE'] ?></th>
										<th><?= $lang['CPC_EARNINGS'] ?></th>
										<th><?= $lang['DATETIME'] ?></th>
									</tr>
									</thead>

									<tbody>
									<?php /* my_referral_table( $start_date, $end_date, ($admin_user=='1' ? 0:$owner) );*/ ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- End Panel -->
				</div>
			</div>
		</div>
		<!-- End Page Content -->

	</div><!-- End Main Wrapper  -->

	<?php include('assets/comp/footer.php'); ?>

<script src='/assets/js/handleDaterangepicker.js'></script>
<script>
	var _startDate=new Date(<?= 1000*strtotime($start_date) ?>), _endDate=new Date(<?= 1000*strtotime($end_date) ?>)
	$(document).ready(function () {
		$('#users').dataTable({
			<?= $cpc_on ? '':'"columnDefs": [{ "targets":[3], "visible":false }],' ?>
			"aaSorting": [[4,'desc']], // def: datetime desc
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "/data-functions/ajax_my-referral-table.php",
			"aoColumns": [{"sName":"1"},{"sName":"2"},{"sName":"3"},{"sName":"4"},{"sName":"x"}]
		});
		handleDateRangePicker('#set-report-range',_startDate,_endDate);
	});
</script>

</body>
</html>
