<?php
include('includes/startup.php');
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();
//REDIRECT ADMIN
if( !$admin_user ) { header( 'Location: dashboard' ); exit; }

include('assets/comp/header.php');
?>

<body>
<!-- Start Top Navigation -->
<?php include('assets/comp/top-nav.php'); ?>
<!-- Start Main Wrapper -->
<div id="wrapper">

	<?php include('assets/comp/side-nav.php'); ?>

	<div id="page-content-wrapper">
		<div class="container-fluid">
			<div class="row">
				<!-- Start Panel -->
				<div class="col-lg-12">
					<div class="panel">
						<div class="panel-heading panel-primary">
							<span class="title"><?php echo $lang['USER_MANAGEMENT']; ?></span>
						</div>
						<div class="panel-content">
							<div>
								<div id="status"></div>
								<table id="users" class="row-border" cellspacing="0" width="100%">
									<thead>
									<tr>
										<th><?= $lang['NAME']; ?></th>
										<th>ID</th>
										<th style='min-width:32px'><?= $lang['JOIN_DATE']; ?></th>
										<th><?= $lang['USERNAME']; ?></th>
										<th><?= $lang['EMAIL']; ?></th>
										<th><?= $lang['WEBSITES']; ?></th>
										<th><?= $lang['ACCEPTED_TERMS']; ?></th>
										<th><?= $lang['APPROVED']; ?></th>
										<th><?= $lang['ADMIN_USER']; ?></th>
										<th><?= $lang['ACTION']; ?></th>
									</tr>
									</thead>
									<tbody>
									<?php user_table(); ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<!-- End Panel -->
				</div>
			</div>
		</div>
		<!-- End Page Content -->

	</div><!-- End Main Wrapper  -->

<?php include('assets/comp/footer.php'); ?>

<script>
	$(document).ready(function () {
		$('#users').DataTable({ "aaSorting": [[2,'desc']], /*def: datetime desc*/ });
/*		$('#users').DataTable({
			"aaSorting": [[2,'desc']], // def: datetime desc
			"processing": true,
			"serverSide": true,
			"sAjaxSource": "/data-functions/ajax_user-management.php",
			"aoColumns": [{"sName":"1"},{"sName":"2"},{"sName":"3"},{"sName":"4"},{"sName":"5"},{"sName":"6"},{"sName":"7"},{"sName":"8"},{"sName":"action","bSortable":false}]
		});*/
	});

	<?php
	if( isset($_SESSION['action_saved']) ) {
		echo 'swal("Awesome", "User privileges have been updated", "success")';
	}
	unset($_SESSION['action_saved']);
	?>

    $("span[contenteditable=true]").blur(function () {
        var id = $(this).data("id");
        var field = $(this).data("field");
        var val = $(this).text();
        $.post('/data-functions/update-user.php', "i="+id+"&f="+field+"&v="+val, function (data) {
            if (data != '') {
               // alert(data);
            }
        });
    });
</script>

</body>
</html>
