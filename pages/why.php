<?php
include( 'includes/startup.php' );
//SITE SETTINGS
list($meta_title, $meta_description, $site_title, $site_email) = all_settings();

include( 'assets/comp/header-guest.php' );
?>
<style>
@media(min-height:1280px) {
	.footer-guest {
		position: fixed;
		bottom: 0;
	}
	body { padding-bottom: 40px; }
}
</style>

<body>
<div class='bodybg'>

	<?php include('assets/comp/top-nav-guest.php'); ?>

	<!-- Page Content -->
	<div class="container">

		<div class="row">
			<div class="center">
				<h1>Top Converting GEO&#39;s</h1>
				<img src='/assets/img/sep-green.png' class='sep' alt='Supported countries'/></a>
			</div>
			<div class="countries-wrap center">
<div><i class='flag flag-bj'></i> Benin</div>
<div><i class='flag flag-bw'></i> Botswana</div>
<div><i class='flag flag-cm'></i> Cameroon</div>
<div><i class='flag flag-td'></i> Chad</div>
<div><i class='flag flag-cg'></i> Congo (Rep.)</div>
<div><i class='flag flag-ci'></i> Cote D`Ivore</div>
<div><i class='flag flag-cg'></i> DR Congo</div>
<div><i class='flag flag-ga'></i> Gabon</div>
<div><i class='flag flag-gh'></i> Ghana</div>
<div><i class='flag flag-gm'></i> Gambia</div>
<div><i class='flag flag-gw'></i> Guinea Bissau</div>
<div><i class='flag flag-gw'></i> Guinea</div>
<div><i class='flag flag-ke'></i> Kenya</div>
<div><i class='flag flag-lr'></i> Liberia</div>
<div><i class='flag flag-ls'></i> Lesotho</div>
<div><i class='flag flag-mg'></i> Madagascar</div>
<div><i class='flag flag-ml'></i> Mali</div>
<div><i class='flag flag-mw'></i> Malawi</div>
<div><i class='flag flag-mz'></i> Mozambique</div>
<div><i class='flag flag-ne'></i> Niger</div>
<div><i class='flag flag-ng'></i> Nigeria</div>
<div><i class='flag flag-rw'></i> Rwanda</div>
<div><i class='flag flag-sl'></i> Sierra Leone</div>
<div><i class='flag flag-sn'></i> Sinegal</div>
<div><i class='flag flag-za'></i> South Africa</div>
<div><i class='flag flag-tg'></i> Togo</div>
<div><i class='flag flag-tz'></i> Tanzania</div>
<div><i class='flag flag-ug'></i> Uganda</div>
<div><i class='flag flag-zm'></i> Zambia</div>
<div><i class='flag flag-zw'></i> Zimbabwe</div>
			</div>
			<div align='center'>
				<input type="button" class="btn btn-warning btn-block register-btn" value="Join Now"
					onclick='location.href="https://affiliates.24monetize.com/signup"'>
			</div>
		</div>

		<div class="row center nopadding">
			<img src='/assets/img/sep-green.png' class='sep' alt='why'/>
			<div class='why-wrap'>
				<div>
					<div class='w1'><img src='/assets/img/why1.png' alt='24/7 Tracking'/></div>
					<div class='t'><h3>24/7 Tracking</h3>You get an easy access to dashboard with all the info you need at one click. Check out our simple tracking solution for reports and visual graphs.</div>
				</div>
				<div>
					<div class='w2'><img src='/assets/img/why2.png' alt='Maximize Your Revenue'/></div>
					<div class='t'><h3>Maximize Your Revenue</h3>We monetize your traffic from around Africa. We have African payment solutions. Get ready for higher conversions from your traffic.</div>
				</div>
				<div>
					<div class='w3'><img src='/assets/img/why3.png' alt='Honesty'/></div>
					<div class='t'><h3>Honesty</h3>Transparency Counts. We know how affiliate programs operate from the inside. We pay lifetime revenue share. We are here to stay so trust us to keep our promises.</div>
				</div>
			</div>
		</div>

	</div><!-- /container -->

</div>

<?php include('assets/comp/footer-guest.php'); ?>

</body>
</html>
