<?php
/*
 * NEW Affiliate
 * redirections, 2020-01-13, Sergey
 */

$_rBase  = 'https://track.24monetize.com';
$_rPubid = '?pubid=';

// 1. URL rules:
// https://www.24monetize.com/lottery/{url}
// to
// https://track.24monetize.com/{CampaignId}
$_rLottery = [ // {url} -> {CampaignId}
	'index'				=> '5e179ec4514b5000019c6f01',
	'el-gordo'			=> '5e17a8db514b5000019c6f24',
	'eurojackpot'		=> '5e17ab50514b5000019c6f33',
	'euromillions'		=> '5e17ad74514b5000019c6f41',
	'euromillions-uk'	=> '5e17aae2514b5000019c6f31',
	'french-loto'		=> '5e17a879514b5000019c6f21',
	'german-lotto'		=> '5e1b1075514b5000019c734c',
	'irish-lotto'		=> '5e17a601514b5000019c6f15',
	'lotto-6-49'		=> '5e17ace4514b5000019c6f3d',
	'mega-millions'		=> '5e17af7d514b5000019c6f4d',
	'mega-sena'			=> '5e17ac99514b5000019c6f3b',
	'oz-lotto'			=> '5e17ad22514b5000019c6f3f',
	'powerball'			=> '5e149124514b5000019c6976',
	'superenalotto'		=> '5e17ac4a514b5000019c6f39',
];

// 2. QS addition rules
// https://www.24monetize.com...?ref={ref}
// to
// https://track.24monetize.com...?pubid={pubid}
$_rRef = [ // {ref} -> {pubid}
	5				=> '5e0345d19016650001846450',
	9				=> '5e0345d2901665000184644b',
	113				=> '5e0345d2901665000184644c',
	127				=> '5e0345d2901665000184644d',
	280				=> '5e0345d3901665000184644e',
	589				=> '5e0345d3901665000184644f',
	696				=> '5e0345d49016650001846450',
	719				=> '5e0345d49016650001846451',
	740				=> '5e0345d49016650001846452',
	783				=> '5e0345d49016650001846453',
	793				=> '5e0345d59016650001846454',
	797				=> '5e0345d59016650001846455',
	803				=> '5e0345d59016650001846456',
	822				=> '5e0345d59016650001846457',
	823				=> '5e0345d69016650001846458',
	824				=> '5e0345d69016650001846459',
	834				=> '5e0345d6901665000184645a',
	844				=> '5e0345d6901665000184645b',
	897				=> '5e0345d6901665000184645c',
	909				=> '5e0345d7901665000184645d',
	919				=> '5e0345d7901665000184645e',
	925				=> '5e0345d7901665000184645f',
	963				=> '5e0345d79016650001846460',
	979				=> '5e0345d89016650001846461',
	985				=> '5e0345d89016650001846462',
	1017			=> '5e0345d89016650001846463',
	1020			=> '5e0345d89016650001846464',
	1071			=> '5e0345d89016650001846465',
	1073			=> '5e0345d99016650001846466',
	1083			=> '5e0345d99016650001846467',
	1097			=> '5e0345d99016650001846468',
	1108			=> '5e0345d99016650001846469',
	1118			=> '5e0345d9901665000184646a',
	1125			=> '5e0345da901665000184646b',
	1128			=> '5e0345da901665000184646c',
	1140			=> '5e0345da901665000184646d',
	1147			=> '5e0345da901665000184646e',
	1149			=> '5e0345db901665000184646f',
	1150			=> '5e0345db9016650001846470',
	1204			=> '5e0345db9016650001846471',
	1215			=> '5e0345db9016650001846472',
	1216			=> '5e0345db9016650001846473',
	1245			=> '5e0345dc9016650001846474',
	1263			=> '5e0345dc9016650001846475',
	1267			=> '5e0345dc9016650001846476',
	1274			=> '5e0345dd9016650001846477',
	1275			=> '5e0345dd9016650001846478',
	1288			=> '5e0345dd9016650001846479',
	1300			=> '5e0345dd901665000184647a',
	1338			=> '5e0345dd901665000184647b',
	1352			=> '5e0345de901665000184647c',
	1368			=> '5e0345de901665000184647d',
	1396			=> '5e0345de901665000184647e',
	1413			=> '5e0345df901665000184647f',
	1506			=> '5e0345df9016650001846480',
	1649			=> '5e0345e09016650001846481',
	1873			=> '5e0345e09016650001846482',
];
//------------------------------------------------


$_lottery = trim( @$_GET['lottery'] );
$_ref     = trim( @$_GET['ref'] );

// for homepage with ref=*
if( $pg=='index' && empty($_lottery) && !empty($_rRef[$_ref]) ) $_lottery = $pg;

$_R = '';
if( !empty($_lottery) && isset($_rLottery[$_lottery]) ) {
	// https://www.24monetize.com/lottery/{url}
	// to
	// https://track.24monetize.com/{CampaignId}
	$_R.= '/'.$_rLottery[$_lottery];
}

if( !empty($_ref) && isset($_rRef[$_ref]) ) {
	// https://www.24monetize.com...?ref={ref}
	// to
	// https://track.24monetize.com...?pubid={pubid}
	$_R.= $_rPubid.$_rRef[$_ref];
}

if( !empty($_R) ) {
	$_R = $_rBase.$_R;
//echo $_R; exit;
	header("HTTP/1.1 301");
	header( "location: $_R" );
	exit;
}
